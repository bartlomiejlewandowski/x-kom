--PostgreSQL Maestro 16.6.0.1
------------------------------------------
--Host     : localhost
--Database : postgres

-- Structure for table algorithm (OID = 16393):
CREATE SCHEMA public;
SET SCHEMA 'public';
SET search_path = public;
CREATE TABLE algorithm (
    id bigint NOT NULL,
    name varchar(255) NOT NULL,
    parameters varchar(20000) NOT NULL,
    "version" integer
) WITHOUT OIDS;
-- Structure for table document (OID = 16401):
CREATE TABLE document (
    id bigint NOT NULL,
    elastic_id varchar(255) NOT NULL,
    metadata text,
    name varchar(255) NOT NULL,
    "version" integer
) WITHOUT OIDS;
-- Structure for table group_rate_detailed_item (OID = 16409):
CREATE TABLE group_rate_detailed_item (
    id bigint NOT NULL,
    ordinal integer NOT NULL,
    rate_value integer NOT NULL,
    "version" integer,
    document bigint,
    group_rate_item bigint,
    phrase bigint,
    rate bigint
) WITHOUT OIDS;
-- Structure for table group_rate_item (OID = 16414):
CREATE TABLE group_rate_item (
    id bigint NOT NULL,
    rate_value numeric(19,2),
    "version" integer,
    group_trial_rate bigint NOT NULL,
    phrase bigint NOT NULL
) WITHOUT OIDS;
-- Structure for table group_trial (OID = 16419):
CREATE TABLE group_trial (
    id bigint NOT NULL,
    cron varchar(255),
    name varchar(255) NOT NULL,
    "version" integer,
    algorithm bigint NOT NULL,
    phrase_group bigint NOT NULL,
    server bigint NOT NULL,
    web_user bigint
) WITHOUT OIDS;
-- Structure for table group_trial_rate (OID = 16427):
CREATE TABLE group_trial_rate (
    id bigint NOT NULL,
    rate_value numeric(19,2),
    run_date timestamp without time zone NOT NULL,
    "version" integer,
    trial bigint NOT NULL,
    web_user bigint NOT NULL
) WITHOUT OIDS;
-- Structure for table phrase (OID = 16432):
CREATE TABLE phrase (
    id bigint NOT NULL,
    text varchar(255) NOT NULL,
    "version" integer,
    web_user bigint
) WITHOUT OIDS;
-- Structure for table phrase_group (OID = 16437):
CREATE TABLE phrase_group (
    id bigint NOT NULL,
    name varchar(255) NOT NULL,
    "version" integer,
    web_user bigint
) WITHOUT OIDS;
-- Structure for table rate (OID = 16442):
CREATE TABLE rate (
    id bigint NOT NULL,
    rate_value integer NOT NULL,
    "version" integer,
    document bigint,
    phrase bigint
) WITHOUT OIDS;
-- Structure for table rate_item (OID = 16447):
CREATE TABLE rate_item (
    id bigint NOT NULL,
    ordinal integer NOT NULL,
    rate_value integer NOT NULL,
    "version" integer,
    document bigint,
    phrase bigint,
    rate bigint,
    trial_rate bigint
) WITHOUT OIDS;
-- Structure for table server (OID = 16452):
CREATE TABLE server (
    id bigint NOT NULL,
    address varchar(255) NOT NULL,
    "index" varchar(255) NOT NULL,
    "password" varchar(255) NOT NULL,
    username varchar(255) NOT NULL,
    "version" integer
) WITHOUT OIDS;
-- Structure for table setting (OID = 16460):
CREATE TABLE setting (
    id bigint NOT NULL,
    description varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    param_value varchar(3000) NOT NULL,
    "version" integer
) WITHOUT OIDS;
-- Structure for table trial (OID = 16468):
CREATE TABLE trial (
    id bigint NOT NULL,
    cron varchar(255),
    name varchar(255) NOT NULL,
    "version" integer,
    algorithm bigint NOT NULL,
    phrase bigint NOT NULL,
    server bigint NOT NULL,
    web_user bigint
) WITHOUT OIDS;
-- Structure for table trial_rate (OID = 16476):
CREATE TABLE trial_rate (
    id bigint NOT NULL,
    rate_value numeric(19,2),
    run_date timestamp without time zone NOT NULL,
    "version" integer,
    trial bigint NOT NULL,
    web_user bigint NOT NULL
) WITHOUT OIDS;
-- Structure for table web_user (OID = 16481):
CREATE TABLE web_user (
    id bigint NOT NULL,
    enabled boolean NOT NULL,
    "password" varchar(255) NOT NULL,
    username varchar(255) NOT NULL,
    "version" integer,
    user_role bigint NOT NULL
) WITHOUT OIDS;
-- Structure for table web_user_role (OID = 16489):
CREATE TABLE web_user_role (
    id bigint NOT NULL,
    name varchar(255) NOT NULL,
    "version" integer
) WITHOUT OIDS;
-- Structure for table weighted_phrase (OID = 16494):
CREATE TABLE weighted_phrase (
    id bigint NOT NULL,
    "version" integer,
    weight numeric(19,2) NOT NULL,
    phrase bigint NOT NULL,
    phrase_group bigint NOT NULL
) WITHOUT OIDS;
-- Structure for table persistent_logins (OID = 16661):
CREATE TABLE persistent_logins (
    username varchar(64) NOT NULL,
    series varchar(64) NOT NULL,
    token varchar(64) NOT NULL,
    last_used timestamp without time zone NOT NULL
) WITHOUT OIDS;
--
-- Data for blobs (OID = 16393) (LIMIT 0,1)
--
INSERT INTO algorithm (id, name, parameters, "version") VALUES (1, 'Domyślny', '{
   "query":{
      "filtered":{
         "query":{
            "bool":{
               "must":[
                  {
                     "function_score":{
                        "query":{
                           "bool":{
                              "must":[
                                 {
                                    "match":{
                                       "fullTitle":{
                                          "query":"%QUERY%",
                                          "fuzziness":0,
                                          "operator":"and"
                                       }
                                    }
                                 }
                              ]
                           }
                        },
                        "functions":[
                           {
                              "script_score":{
                                 "script":"(1.0 + (doc[''boostFactor''].value / 100))",
                                 "params":[

                                 ]
                              }
                           }
                        ]
                     }
                  }
               ]
            }
         }
      }
   },
   "aggregations":{
      "agg_count":{
         "value_count":{
            "field":"catalogProductID"
         }
      },
      "agg_visibility":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "terms":{
                        "groupID":[
                           5
                        ]
                     }
                  },
                  {
                     "terms":{
                        "parentCategoryID":[
                           1713
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_visibility_features":{
               "terms":{
                  "field":"features",
                  "size":10000
               }
            },
            "agg_visibility_producent":{
               "terms":{
                  "field":"producentID",
                  "size":10000
               }
            },
            "agg_visibility_category":{
               "terms":{
                  "field":"parentCategoryID",
                  "size":10000
               }
            },
            "agg_visibility_groupID":{
               "terms":{
                  "field":"groupID",
                  "size":10000
               }
            },
            "agg_visibility_featureGroupID":{
               "terms":{
                  "field":"featureGroupID",
                  "size":10000
               }
            }
         }
      },
      "agg_other":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "range":{
                        "price_0":{
                           "from":300,
                           "to":null
                        }
                     }
                  },
                  {
                     "terms":{
                        "groupID":[
                           5
                        ]
                     }
                  },
                  {
                     "terms":{
                        "parentCategoryID":[
                           1713
                        ]
                     }
                  },
                  {
                     "terms":{
                        "producentID":[
                           27
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           26474
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           25054
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_other_features":{
               "terms":{
                  "field":"features",
                  "size":10000
               }
            },
            "agg_other_producent":{
               "terms":{
                  "field":"producentID",
                  "size":10000
               }
            },
            "agg_other_category":{
               "terms":{
                  "field":"categoryID",
                  "size":10000
               }
            },
            "agg_other_parentCategory":{
               "terms":{
                  "field":"parentCategoryID",
                  "size":10000
               }
            },
            "agg_other_groupID":{
               "terms":{
                  "field":"groupID",
                  "size":10000
               }
            }
         }
      },
      "agg_category":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "range":{
                        "price_0":{
                           "from":300,
                           "to":null
                        }
                     }
                  },
                  {
                     "terms":{
                        "producentID":[
                           27
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           26474
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           25054
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_category_category":{
               "terms":{
                  "field":"categoryID",
                  "size":10000
               }
            },
            "agg_category_parentCategory":{
               "terms":{
                  "field":"parentCategoryID",
                  "size":10000
               }
            },
            "agg_category_groupID":{
               "terms":{
                  "field":"groupID",
                  "size":10000
               }
            }
         }
      },
      "agg_producent":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "range":{
                        "price_0":{
                           "from":300,
                           "to":null
                        }
                     }
                  },
                  {
                     "terms":{
                        "groupID":[
                           5
                        ]
                     }
                  },
                  {
                     "terms":{
                        "parentCategoryID":[
                           1713
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           26474
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           25054
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_producent_producent":{
               "terms":{
                  "field":"producentID",
                  "size":10000
               }
            }
         }
      },
      "agg_feature_1759":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "range":{
                        "price_0":{
                           "from":300,
                           "to":null
                        }
                     }
                  },
                  {
                     "terms":{
                        "groupID":[
                           5
                        ]
                     }
                  },
                  {
                     "terms":{
                        "parentCategoryID":[
                           1713
                        ]
                     }
                  },
                  {
                     "terms":{
                        "producentID":[
                           27
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           25054
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_feature_1759_features":{
               "terms":{
                  "field":"featuresExt",
                  "size":10000,
                  "include":"1759_.*"
               }
            }
         }
      },
      "agg_feature_1765":{
         "filter":{
            "bool":{
               "must":[
                  {
                     "range":{
                        "price_0":{
                           "from":300,
                           "to":null
                        }
                     }
                  },
                  {
                     "terms":{
                        "groupID":[
                           5
                        ]
                     }
                  },
                  {
                     "terms":{
                        "parentCategoryID":[
                           1713
                        ]
                     }
                  },
                  {
                     "terms":{
                        "producentID":[
                           27
                        ]
                     }
                  },
                  {
                     "terms":{
                        "features":[
                           26474
                        ]
                     }
                  }
               ]
            }
         },
         "aggregations":{
            "agg_feature_1765_features":{
               "terms":{
                  "field":"featuresExt",
                  "size":10000,
                  "include":"1765_.*"
               }
            }
         }
      }
   },
   "from":0,
   "size":30
}', 7);
COMMIT;
--
-- Data for blobs (OID = 16452) (LIMIT 0,1)
--
INSERT INTO server (id, address, "index", "password", username, "version") VALUES (1, 'https://sopxkomsearch04.cloudapp.net:8889', 'products_ops', 'hF20sRVc7QoiPrrH', 'elasticguest', 2);
COMMIT;
--
-- Data for blobs (OID = 16460) (LIMIT 0,2)
--
INSERT INTO setting (id, description, name, param_value, "version") VALUES (1, 'Wartości wag dla kolejnych wyników', 'result_weights', '1
0.53
0.46
0.33
0.285
0.264
0.223
0.2
0.188
0.168
0.155
0.141
0.122
0.114
0.113
0.112
0.111
0.104
0.085
0.077
0.073
0.07
0.068
0.065
0.063
0.059
0.056
0.053
0.051
0.048
0.048
0.047
0.047
0.047
0.046
0.046
0.046
0.046
0.046
0.045
0.045
0.045
0.045
0.045
0.044
0.044
0.044
0.044
0.044
0.044
0.043
0.043
0.043
0.043
0.043
0.043
0.043
0.042
0.042
0.042
0.042
0.042
0.042
0.042
0.042
0.041
0.041
0.041
0.041
0.041
0.041
0.041
0.041
0.041
0.04
0.04
0.04
0.04
0.04
0.04
0.04
0.04
0.04
0.04
0.039
0.039
0.039
0.039
0.039
0.039
0.039
0.039
0.039
0.039
0.039
0.038
0.038
0.038
0.038
0.038
0.02', 3);
INSERT INTO setting (id, description, name, param_value, "version") VALUES (2, 'Maksymalna liczba wyników dla zapytania, które zostaną zapisane', 'max_results', '10', 4);
COMMIT;
--
-- Data for blobs (OID = 16481) (LIMIT 0,2)
--
INSERT INTO web_user (id, enabled, "password", username, "version", user_role) VALUES (1, true, '70c2e1a03115060f007532bd3532ece9ec21e8558623eec4fcaba854bcc65578', 'findwise', 0, 2);
INSERT INTO web_user (id, enabled, "password", username, "version", user_role) VALUES (2, true, '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin', 0, 1);
COMMIT;
--
-- Data for blobs (OID = 16489) (LIMIT 0,2)
--
INSERT INTO web_user_role (id, name, "version") VALUES (1, 'ROLE_ADMIN', 0);
INSERT INTO web_user_role (id, name, "version") VALUES (2, 'ROLE_USER', 0);
COMMIT;
-- Definition for index algorithm_pkey (OID = 16399):
ALTER TABLE ONLY algorithm
    ADD CONSTRAINT algorithm_pkey PRIMARY KEY (id);
-- Definition for index document_pkey (OID = 16407):
ALTER TABLE ONLY document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id);
-- Definition for index group_rate_detailed_item_pkey (OID = 16412):
ALTER TABLE ONLY group_rate_detailed_item
    ADD CONSTRAINT group_rate_detailed_item_pkey PRIMARY KEY (id);
-- Definition for index group_rate_item_pkey (OID = 16417):
ALTER TABLE ONLY group_rate_item
    ADD CONSTRAINT group_rate_item_pkey PRIMARY KEY (id);
-- Definition for index group_trial_pkey (OID = 16425):
ALTER TABLE ONLY group_trial
    ADD CONSTRAINT group_trial_pkey PRIMARY KEY (id);
-- Definition for index group_trial_rate_pkey (OID = 16430):
ALTER TABLE ONLY group_trial_rate
    ADD CONSTRAINT group_trial_rate_pkey PRIMARY KEY (id);
-- Definition for index phrase_pkey (OID = 16435):
ALTER TABLE ONLY phrase
    ADD CONSTRAINT phrase_pkey PRIMARY KEY (id);
-- Definition for index phrase_group_pkey (OID = 16440):
ALTER TABLE ONLY phrase_group
    ADD CONSTRAINT phrase_group_pkey PRIMARY KEY (id);
-- Definition for index rate_pkey (OID = 16445):
ALTER TABLE ONLY rate
    ADD CONSTRAINT rate_pkey PRIMARY KEY (id);
-- Definition for index rate_item_pkey (OID = 16450):
ALTER TABLE ONLY rate_item
    ADD CONSTRAINT rate_item_pkey PRIMARY KEY (id);
-- Definition for index server_pkey (OID = 16458):
ALTER TABLE ONLY server
    ADD CONSTRAINT server_pkey PRIMARY KEY (id);
-- Definition for index setting_pkey (OID = 16466):
ALTER TABLE ONLY setting
    ADD CONSTRAINT setting_pkey PRIMARY KEY (id);
-- Definition for index trial_pkey (OID = 16474):
ALTER TABLE ONLY trial
    ADD CONSTRAINT trial_pkey PRIMARY KEY (id);
-- Definition for index trial_rate_pkey (OID = 16479):
ALTER TABLE ONLY trial_rate
    ADD CONSTRAINT trial_rate_pkey PRIMARY KEY (id);
-- Definition for index web_user_pkey (OID = 16487):
ALTER TABLE ONLY web_user
    ADD CONSTRAINT web_user_pkey PRIMARY KEY (id);
-- Definition for index web_user_role_pkey (OID = 16492):
ALTER TABLE ONLY web_user_role
    ADD CONSTRAINT web_user_role_pkey PRIMARY KEY (id);
-- Definition for index weighted_phrase_pkey (OID = 16497):
ALTER TABLE ONLY weighted_phrase
    ADD CONSTRAINT weighted_phrase_pkey PRIMARY KEY (id);
-- Definition for index uk_b5stnmwny6pdi7pmepk2917m3 (OID = 16499):
ALTER TABLE ONLY document
    ADD CONSTRAINT uk_b5stnmwny6pdi7pmepk2917m3 UNIQUE (elastic_id);
-- Definition for index uk_rtolxrjqts0bvp0f6k2c7p3o6 (OID = 16501):
ALTER TABLE ONLY phrase
    ADD CONSTRAINT uk_rtolxrjqts0bvp0f6k2c7p3o6 UNIQUE (text);
-- Definition for index uk_s7f7i23dnn5919qmu5y3xqdpt (OID = 16503):
ALTER TABLE ONLY phrase_group
    ADD CONSTRAINT uk_s7f7i23dnn5919qmu5y3xqdpt UNIQUE (name);
-- Definition for index uk_t4brugp1vi13ey600vpgxf38i (OID = 16505):
ALTER TABLE ONLY rate
    ADD CONSTRAINT uk_t4brugp1vi13ey600vpgxf38i UNIQUE (phrase, document);
-- Definition for index uk_bk4oycm648x0ox633r4m22b7d (OID = 16507):
ALTER TABLE ONLY setting
    ADD CONSTRAINT uk_bk4oycm648x0ox633r4m22b7d UNIQUE (name);
-- Definition for index uk_sydf5vujahmtb782b5tycd0h4 (OID = 16509):
ALTER TABLE ONLY web_user
    ADD CONSTRAINT uk_sydf5vujahmtb782b5tycd0h4 UNIQUE (username);
-- Definition for index uk_nbf66txbvd4oj1vi6ong41100 (OID = 16511):
ALTER TABLE ONLY weighted_phrase
    ADD CONSTRAINT uk_nbf66txbvd4oj1vi6ong41100 UNIQUE (phrase, phrase_group);
-- Definition for index fk_fv23irvejwqp9tmoomso9pmj3 (OID = 16513):
ALTER TABLE ONLY group_rate_detailed_item
    ADD CONSTRAINT fk_fv23irvejwqp9tmoomso9pmj3 FOREIGN KEY (document) REFERENCES document(id);
-- Definition for index fk_pqtluabjr4ps7e6f6rrlophpo (OID = 16518):
ALTER TABLE ONLY group_rate_detailed_item
    ADD CONSTRAINT fk_pqtluabjr4ps7e6f6rrlophpo FOREIGN KEY (group_rate_item) REFERENCES group_rate_item(id);
-- Definition for index fk_k2h88xq1hu4w2mfxlbtx7oc87 (OID = 16523):
ALTER TABLE ONLY group_rate_detailed_item
    ADD CONSTRAINT fk_k2h88xq1hu4w2mfxlbtx7oc87 FOREIGN KEY (phrase) REFERENCES weighted_phrase(id);
-- Definition for index fk_hvlktcwnnsvqpwlsya94613sk (OID = 16528):
ALTER TABLE ONLY group_rate_detailed_item
    ADD CONSTRAINT fk_hvlktcwnnsvqpwlsya94613sk FOREIGN KEY (rate) REFERENCES rate(id);
-- Definition for index fk_gh0eary3lgoyslkhq9ijsxel6 (OID = 16533):
ALTER TABLE ONLY group_rate_item
    ADD CONSTRAINT fk_gh0eary3lgoyslkhq9ijsxel6 FOREIGN KEY (group_trial_rate) REFERENCES group_trial_rate(id);
-- Definition for index fk_45beu92to1s0524lwg2pgoaw9 (OID = 16538):
ALTER TABLE ONLY group_rate_item
    ADD CONSTRAINT fk_45beu92to1s0524lwg2pgoaw9 FOREIGN KEY (phrase) REFERENCES weighted_phrase(id);
-- Definition for index fk_l0j9givnukhwmep248capheg6 (OID = 16543):
ALTER TABLE ONLY group_trial
    ADD CONSTRAINT fk_l0j9givnukhwmep248capheg6 FOREIGN KEY (algorithm) REFERENCES algorithm(id);
-- Definition for index fk_65psgi8ttje6ascvxpcqtf2y0 (OID = 16548):
ALTER TABLE ONLY group_trial
    ADD CONSTRAINT fk_65psgi8ttje6ascvxpcqtf2y0 FOREIGN KEY (phrase_group) REFERENCES phrase_group(id);
-- Definition for index fk_nf916dekigycs2gb5m4qdm7p0 (OID = 16553):
ALTER TABLE ONLY group_trial
    ADD CONSTRAINT fk_nf916dekigycs2gb5m4qdm7p0 FOREIGN KEY (server) REFERENCES server(id);
-- Definition for index fk_5jjnfevqfe593qxcjygvj09wf (OID = 16558):
ALTER TABLE ONLY group_trial
    ADD CONSTRAINT fk_5jjnfevqfe593qxcjygvj09wf FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_4w8y0w1fbhyi9ikf8hywq94ne (OID = 16563):
ALTER TABLE ONLY group_trial_rate
    ADD CONSTRAINT fk_4w8y0w1fbhyi9ikf8hywq94ne FOREIGN KEY (trial) REFERENCES group_trial(id);
-- Definition for index fk_t93cy9kdsvmbroaso0naue7qt (OID = 16568):
ALTER TABLE ONLY group_trial_rate
    ADD CONSTRAINT fk_t93cy9kdsvmbroaso0naue7qt FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_chvksyf25joasmvlr3k6y4yov (OID = 16573):
ALTER TABLE ONLY phrase
    ADD CONSTRAINT fk_chvksyf25joasmvlr3k6y4yov FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_cca7vi9dje22fs0vjagv9lpqe (OID = 16578):
ALTER TABLE ONLY phrase_group
    ADD CONSTRAINT fk_cca7vi9dje22fs0vjagv9lpqe FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_ns9hv2ighupq20n8y8grk2nop (OID = 16583):
ALTER TABLE ONLY rate
    ADD CONSTRAINT fk_ns9hv2ighupq20n8y8grk2nop FOREIGN KEY (document) REFERENCES document(id);
-- Definition for index fk_j65q63c9b51oo8a1hlcla9dsi (OID = 16588):
ALTER TABLE ONLY rate
    ADD CONSTRAINT fk_j65q63c9b51oo8a1hlcla9dsi FOREIGN KEY (phrase) REFERENCES phrase(id);
-- Definition for index fk_m7qh1gvow4gojd6mmy41tijst (OID = 16593):
ALTER TABLE ONLY rate_item
    ADD CONSTRAINT fk_m7qh1gvow4gojd6mmy41tijst FOREIGN KEY (document) REFERENCES document(id);
-- Definition for index fk_ndylpaqlpmq263ptknsq4tfwo (OID = 16598):
ALTER TABLE ONLY rate_item
    ADD CONSTRAINT fk_ndylpaqlpmq263ptknsq4tfwo FOREIGN KEY (phrase) REFERENCES phrase(id);
-- Definition for index fk_i6vhffsh5mytoonfb8fgskqvf (OID = 16603):
ALTER TABLE ONLY rate_item
    ADD CONSTRAINT fk_i6vhffsh5mytoonfb8fgskqvf FOREIGN KEY (rate) REFERENCES rate(id);
-- Definition for index fk_3nr9vay8rl4pdbd5ttni8cq56 (OID = 16608):
ALTER TABLE ONLY rate_item
    ADD CONSTRAINT fk_3nr9vay8rl4pdbd5ttni8cq56 FOREIGN KEY (trial_rate) REFERENCES trial_rate(id);
-- Definition for index fk_f3xrkq548orubc9iuinw2l35c (OID = 16613):
ALTER TABLE ONLY trial
    ADD CONSTRAINT fk_f3xrkq548orubc9iuinw2l35c FOREIGN KEY (algorithm) REFERENCES algorithm(id);
-- Definition for index fk_k5iibioplh2oopuv0c9cs810k (OID = 16618):
ALTER TABLE ONLY trial
    ADD CONSTRAINT fk_k5iibioplh2oopuv0c9cs810k FOREIGN KEY (phrase) REFERENCES phrase(id);
-- Definition for index fk_elrldn9guepeno1gg17p2l999 (OID = 16623):
ALTER TABLE ONLY trial
    ADD CONSTRAINT fk_elrldn9guepeno1gg17p2l999 FOREIGN KEY (server) REFERENCES server(id);
-- Definition for index fk_f3w3bvskkh5b777dx9d09r69n (OID = 16628):
ALTER TABLE ONLY trial
    ADD CONSTRAINT fk_f3w3bvskkh5b777dx9d09r69n FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_psv6wylg7a2d0h4xqgcipv9o0 (OID = 16633):
ALTER TABLE ONLY trial_rate
    ADD CONSTRAINT fk_psv6wylg7a2d0h4xqgcipv9o0 FOREIGN KEY (trial) REFERENCES trial(id);
-- Definition for index fk_60r71huhrb116p0lnijkb4l85 (OID = 16638):
ALTER TABLE ONLY trial_rate
    ADD CONSTRAINT fk_60r71huhrb116p0lnijkb4l85 FOREIGN KEY (web_user) REFERENCES web_user(id);
-- Definition for index fk_lx5yepfsijyc7p6s47sunt7u3 (OID = 16643):
ALTER TABLE ONLY web_user
    ADD CONSTRAINT fk_lx5yepfsijyc7p6s47sunt7u3 FOREIGN KEY (user_role) REFERENCES web_user_role(id);
-- Definition for index fk_sipqqkivvysr00rc0rgjq783c (OID = 16648):
ALTER TABLE ONLY weighted_phrase
    ADD CONSTRAINT fk_sipqqkivvysr00rc0rgjq783c FOREIGN KEY (phrase) REFERENCES phrase(id);
-- Definition for index fk_2uh8mq7vre8f5vykb4bvrbojh (OID = 16653):
ALTER TABLE ONLY weighted_phrase
    ADD CONSTRAINT fk_2uh8mq7vre8f5vykb4bvrbojh FOREIGN KEY (phrase_group) REFERENCES phrase_group(id);
-- Definition for index persistent_logins_pkey (OID = 16664):
ALTER TABLE ONLY persistent_logins
    ADD CONSTRAINT persistent_logins_pkey PRIMARY KEY (series);
SET search_path = pg_catalog, pg_catalog;
COMMENT ON SCHEMA public IS 'standard public schema';
