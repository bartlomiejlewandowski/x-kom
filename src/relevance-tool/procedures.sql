-- //////////////////////////////////////////////////////////////////////////////////////////////////
-- Read rate weights from configuration
-- //////////////////////////////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION read_weights() RETURNS float8[] AS
$BODY$
DECLARE
  weights float8[];
BEGIN
  SELECT regexp_split_to_array(param_value, '\r?\n') 
    FROM setting WHERE name = 'result_weights' INTO weights;
  RETURN weights;
END;
$BODY$
LANGUAGE plpgsql STABLE;

-- //////////////////////////////////////////////////////////////////////////////////////////////////
-- Get weight for position
-- //////////////////////////////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION weight_for_position(IN ordinal_no integer) 
RETURNS float8 AS
$BODY$
DECLARE
  weights float8[];
BEGIN

  -- get weights from configuration
  weights := read_weights();

  IF ordinal_no > array_length(weights, 1) THEN
    RETURN weights[weights_size];
  ELSE 
    RETURN weights[ordinal_no];
  END IF;

END;
$BODY$
LANGUAGE plpgsql STABLE;

-- //////////////////////////////////////////////////////////////////////////////////////////////////
-- Create materialized views
-- //////////////////////////////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION create_views() RETURNS void AS
$BODY$
BEGIN

  -- create view for trial rates
  CREATE MATERIALIZED VIEW IF NOT EXISTS rate_item_rates AS
    SELECT 
      rate_value, 
      ordinal, 
      (rate_value * weight_for_position(ordinal)) weighted_value 
    FROM rate_item 
    GROUP BY rate_value, ordinal;
  CREATE UNIQUE INDEX IF NOT EXISTS rate_item_rates_index ON rate_item_rates (rate_value, ordinal);

  -- create view for group detailed rates
  CREATE MATERIALIZED VIEW IF NOT EXISTS group_detailed_rates AS
    SELECT 
      grdi.rate_value, 
      grdi.ordinal, 
      grdi.phrase, 
      (grdi.rate_value * weight_for_position(grdi.ordinal) * wp.weight) weighted_value 
    FROM group_rate_detailed_item grdi 
      JOIN weighted_phrase wp ON wp.id = grdi.phrase
    GROUP BY grdi.rate_value, grdi.ordinal, grdi.phrase, wp.weight;
  CREATE UNIQUE INDEX IF NOT EXISTS group_detailed_rates_index ON group_detailed_rates (rate_value, ordinal, phrase);

END;
$BODY$
LANGUAGE plpgsql;

-- //////////////////////////////////////////////////////////////////////////////////////////////////
-- Update trial rate values for given recently updated rates
-- //////////////////////////////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION update_trial_rates(IN rate_ids bigint[]) RETURNS void AS
$BODY$
BEGIN

  -- select rate items to update
  CREATE TEMP TABLE update_table ON COMMIT DROP AS
    SELECT rate_item.id item_id, trial_rate.id rate_id, rate.rate_value FROM rate 
      JOIN rate_item ON rate.id = rate_item.rate 
      JOIN trial_rate ON trial_rate.id = rate_item.trial_rate 
    WHERE rate.id = ANY(rate_ids);

  -- update rate item values
  UPDATE rate_item SET rate_value = update_info.rate_value FROM (
    SELECT item_id, rate_value FROM update_table
  ) AS update_info WHERE rate_item.id = update_info.item_id;

  -- refresh rates view
  REFRESH MATERIALIZED VIEW rate_item_rates;

  -- update trial rates
  UPDATE trial_rate SET rate_value = (
    SELECT SUM(rates.weighted_value) FROM rate_item 
      JOIN rate_item_rates rates ON 
        rate_item.rate_value = rates.rate_value AND rate_item.ordinal = rates.ordinal
    WHERE trial_rate = trial_rate.id
  )
  WHERE id IN (SELECT DISTINCT rate_id FROM update_table);

END;
$BODY$
LANGUAGE plpgsql;

-- //////////////////////////////////////////////////////////////////////////////////////////////////
-- Update group trial rate values for given recently updated rates
-- //////////////////////////////////////////////////////////////////////////////////////////////////
CREATE OR REPLACE FUNCTION update_group_trial_rates(IN rate_ids bigint[]) RETURNS void AS
$BODY$
BEGIN

  -- select group detailed rate items to update
  CREATE TEMP TABLE group_update_table ON COMMIT DROP AS
    SELECT group_rate_detailed_item.id detailed_id, 
      group_rate_item.id item_id, 
      group_trial_rate.id rate_id, 
      rate.rate_value 
      FROM rate 
      JOIN group_rate_detailed_item ON rate.id = group_rate_detailed_item.rate 
      JOIN group_rate_item ON group_rate_item.id = group_rate_detailed_item.group_rate_item
      JOIN group_trial_rate ON group_trial_rate.id = group_rate_item.group_trial_rate
    WHERE rate.id = ANY(rate_ids);

  -- update group rate detailed item values
  UPDATE group_rate_detailed_item SET rate_value = update_info.rate_value FROM (
    SELECT detailed_id, rate_value FROM group_update_table
  ) AS update_info WHERE group_rate_detailed_item.id = update_info.detailed_id;

  -- refresh rates view
  REFRESH MATERIALIZED VIEW group_detailed_rates;

  -- update group rate items
  UPDATE group_rate_item SET rate_value = (
    SELECT SUM(rates.weighted_value) FROM group_rate_detailed_item grdi
      JOIN group_detailed_rates rates ON 
        grdi.rate_value = rates.rate_value AND grdi.ordinal = rates.ordinal AND grdi.phrase = rates.phrase
    WHERE grdi.group_rate_item = group_rate_item.id
  ) WHERE id IN (SELECT DISTINCT item_id FROM group_update_table);

  -- update group trial rates
  UPDATE group_trial_rate SET rate_value = results.rate_value FROM (
    SELECT group_trial_rate.id, AVG(group_rate_item.rate_value) rate_value FROM group_trial_rate 
      JOIN group_rate_item ON group_trial_rate.id = group_rate_item.group_trial_rate 
    WHERE group_trial_rate.id IN (SELECT DISTINCT rate_id FROM group_update_table)
    GROUP BY group_trial_rate.id
  ) AS results WHERE group_trial_rate.id = results.id;

END;
$BODY$
LANGUAGE plpgsql;

-- /////////////////////////////////////////////////////////////////;/////////////////////////////////
-- Initialize materialized views
-- //////////////////////////////////////////////////////////////////////////////////////////////////
SELECT create_views();
