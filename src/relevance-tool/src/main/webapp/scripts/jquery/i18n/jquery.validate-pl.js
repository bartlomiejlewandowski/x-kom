/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EN (English)
 */
(function ($) {
	$.extend($.validator.messages, {
		required: "pole wymagane",
    remote: "Popraw pole",
    email: "Podaj poprawny adres e-mail",
    url: "Niepoprawny adres URL",
    date: "Podaj poprawną datę.",
    dateISO: "Podaj poprawną datę (ISO).",
    number: "Wprowadź liczbę.",
    digits: "Tylko cyfry.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    maxlength: $.validator.format("Nie więcej niż {0} znaków."),
    minlength: $.validator.format("Przynajmniej {0} znaków."),
    rangelength: $.validator.format("Wprowadź tekst o długości między {0} a {1} znaków."),
    range: $.validator.format("Waga musi nale\u017Ce\u0107 do przedzia\u0142u od {2} do {1}"),
    max: $.validator.format("Please enter a value less than or equal to {0}."),
    min: $.validator.format("Please enter a value greater than or equal to {0}.")
	});
}(jQuery));