$(function() {

  // bind clear cache event
  $(".clear_cache").click(function() {
    window.localStorage.clear();
  });

  // make current menu item active
  var url = window.location.href.replace(/(http:\/\/|&datatablesRedirect=.*)/g, "");
  var linkUrl = url.substr(url.indexOf("/"));
  $("#sidebar-wrapper ul li a").each(function(){
	var href = $(this).attr("href");
    if(linkUrl === href) {
      $(this).addClass("active");
    }
  });

  // setup multiselect
  $("select[multiple=true]").each(function(){
	$(this).multiSelect();
  });

  // format JSON
  $(".json-format").each(function(){
	$(this).jsonFormatter();
  });

});
