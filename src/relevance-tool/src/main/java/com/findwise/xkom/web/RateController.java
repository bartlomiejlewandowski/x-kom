package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Rate;
import com.findwise.xkom.model.RateBatchService;
import com.findwise.xkom.services.RateService;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

@RequestMapping("/rates")
@Controller
@RooWebScaffold(path = "rates", formBackingObject = Rate.class)
@GvNIXWebJpaBatch(service = RateBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, inlineEditing = true)
@GvNIXLoupeController
public class RateController {

    @Autowired
    private RateService rateService;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Rate rate, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors() || rate.getDocument() == null || rate.getPhrase() == null) {
            populateEditForm(uiModel, rate);
            return "rates/create";
        }
        uiModel.asMap().clear();
        rateService.storeRate(rate);
        return "redirect:/rates/" + encodeUrlPathSegment(rate.getId().toString(), httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Rate rate, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, rate);
            return "rates/update";
        }
        uiModel.asMap().clear();
        Rate oldRate = Rate.findRate(rate.getId());
        rate.setDocument(oldRate.getDocument());
        rate.setPhrase(oldRate.getPhrase());
        rate.merge();
        return "redirect:/rates/" + encodeUrlPathSegment(rate.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "Rate_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");

        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Rate());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Phrase.countPhrases() == 0) {
            dependencies.add(new String[] { "phrase", "phrases" });
        }
        if (Document.countDocuments() == 0) {
            dependencies.add(new String[] { "document", "documents" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        uiModel.addAttribute("rateValues", rateService.populateRateValues());
        return "rates/create";
    }

    public void populateItemForRender(HttpServletRequest request, Rate rate, boolean editing) {
        org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();

        request.setAttribute("rate", rate);
        request.setAttribute("itemId", conversionService_dtt.convert(rate.getId(), String.class));

        if (editing) {
            // spring from:input tag uses BindingResult to locate property
            // editors for each bean
            // property. So, we add a request attribute (required key id
            // BindingResult.MODEL_KEY_PREFIX + object name)
            // with a correctly initialized bindingResult.
            BeanPropertyBindingResult bindindResult = new BeanPropertyBindingResult(rate, "rate");
            bindindResult.initConversion(conversionService_dtt);
            request.setAttribute(BindingResult.MODEL_KEY_PREFIX + "rate", bindindResult);
            // Add date time patterns and enums to populate inputs
            populateEditForm(uiModel, rate);
        }

        // Load uiModel attributes into request
        Map<String, Object> modelMap = uiModel.asMap();
        for (String key : modelMap.keySet()) {
            request.setAttribute(key, modelMap.get(key));
        }
        request.setAttribute("rateValues", rateService.populateRateValues());
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllRates(@DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute Rate rate, HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        Document document = rate.getDocument();
        Phrase phrase = rate.getPhrase();
        String search = Strings.isNullOrEmpty(criterias.getSearch()) ? null : criterias.getSearch();
        TypedQuery<Rate> query = null;
        Long count = 0l;
        // note that filtering on both document and phrase is not supported (not
        // used in app)
        String[] searchFields = new String[] { "document.name", "phrase.text" };
        if (document == null && phrase == null && search == null) {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Rate AS o");
            QueryUtils.appendOrderClause(queryBuilder, Rate.fieldNames4OrderClauseFilter, sortFieldName, sortOrder);
            query = Rate.entityManager().createQuery(queryBuilder.toString(), Rate.class);
            count = Rate.countRates();
        } else if (document != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("document", document);
            Pair<TypedQuery<Rate>, Long> resultInfo = QueryUtils.getResultInfo(Rate.entityManager(), "Rate", filters,
                    searchFields, search, Rate.fieldNames4OrderClauseFilter, sortFieldName, sortOrder, Rate.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (document != null && search == null) {
            query = Rate.findRatesByDocument(document, sortFieldName, sortOrder);
            count = Rate.countFindRatesByDocument(document);
        } else if (phrase != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("phrase", phrase);
            Pair<TypedQuery<Rate>, Long> resultInfo = QueryUtils.getResultInfo(Rate.entityManager(), "Rate", filters,
                    searchFields, search, Rate.fieldNames4OrderClauseFilter, sortFieldName, sortOrder, Rate.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (phrase != null && search == null) {
            query = Rate.findRatesByPhrase(phrase, sortFieldName, sortOrder);
            count = Rate.countFindRatesByPhrase(phrase);
        } else {
            Map<String, Object> filters = Maps.newHashMap();
            Pair<TypedQuery<Rate>, Long> resultInfo = QueryUtils.getResultInfo(Rate.entityManager(), "Rate", filters,
                    searchFields, search, Rate.fieldNames4OrderClauseFilter, sortFieldName, sortOrder, Rate.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }

    void populateEditForm(Model uiModel, Rate rate) {
        uiModel.addAttribute("rate", rate);
    }
}
