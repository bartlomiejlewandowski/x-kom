package com.findwise.xkom.web;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.model.WebUserBatchService;
import com.findwise.xkom.utils.EncryptionUtils;

@RequestMapping("/webusers")
@Controller
@RooWebScaffold(path = "webusers", formBackingObject = WebUser.class)
@GvNIXWebJpaBatch(service = WebUserBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(inlineEditing = false, ajax = true)
@GvNIXLoupeController
public class WebUserController {

    private static final String OLD_PASSWORD = "old_password";

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid WebUser webUser, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, webUser);
            return "webusers/create";
        }
        hashPassword(webUser, null);
        uiModel.asMap().clear();
        webUser.persist();
        return "redirect:/webusers/" + encodeUrlPathSegment(webUser.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        WebUser user = WebUser.findWebUser(id);
        uiModel.addAttribute(OLD_PASSWORD, user.getPassword());
        populateEditForm(uiModel, user);
        return "webusers/update";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid WebUser webUser, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, webUser);
            return "webusers/update";
        }
        hashPassword(webUser, httpServletRequest);
        uiModel.asMap().clear();
        webUser.merge();
        return "redirect:/webusers/" + encodeUrlPathSegment(webUser.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "WebUser_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");

        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }

    private void hashPassword(WebUser user, HttpServletRequest httpServletRequest) {
        try {
            if (httpServletRequest != null && StringUtils.isEmpty(user.getPassword())) {
                user.setPassword(httpServletRequest.getParameter(OLD_PASSWORD).toString());
            } else {
                String hashedPassword = EncryptionUtils.sha256(user.getPassword());
                user.setPassword(hashedPassword);
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            // it shouldn't happen
            e.printStackTrace();
        }
    }
}
