package com.findwise.xkom.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.web.multipart.MultipartFile;

public class PhraseImportUtils extends ImportUtils {

    private static final int COLUMNS_COUNT = 3;

    public static final String WITHOUT_GROUP = "no_group";

    /**
     * Create phrases map (group name -> pair(phrase text, weight))
     * 
     * @param file
     *            import file
     * @return phrases map
     * @throws IOException
     */
    public static Map<String, List<Pair<String, BigDecimal>>> getPhrasesMap(MultipartFile file) throws IOException {
        Map<String, List<Pair<String, BigDecimal>>> map = new HashMap<>();
        List<CSVRecord> records = getRecords(file);
        for (CSVRecord record : records) {
            if (record.size() == COLUMNS_COUNT) {
                String groupName = record.get(0).isEmpty() ? WITHOUT_GROUP : record.get(0);
                String phraseText = record.get(1);
                BigDecimal phraseWeight = record.get(2).isEmpty() ? BigDecimal.valueOf(0.5)
                        : BigDecimal.valueOf(Double.parseDouble(record.get(2).replace(',', '.')));
                if (!map.containsKey(groupName)) {
                    map.put(groupName, new ArrayList<Pair<String, BigDecimal>>());
                }
                map.get(groupName).add(new ImmutablePair<String, BigDecimal>(phraseText, phraseWeight));
            }
        }
        return map;
    }

    public static boolean hasValidContent(MultipartFile file) {
        return hasValidContent(COLUMNS_COUNT, file, false);
    }
}
