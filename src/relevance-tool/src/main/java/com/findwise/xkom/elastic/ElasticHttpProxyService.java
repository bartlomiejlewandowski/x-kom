package com.findwise.xkom.elastic;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.findwise.xkom.config.Configuration;
import com.findwise.xkom.model.Algorithm;
import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Server;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;

@Service
public class ElasticHttpProxyService implements ElasticProxyService {

    private static final Logger log = Logger.getLogger(ElasticHttpProxyService.class);
    private static final String SEARCH_URL_FORMAT = "%s/%s/_search";
    private static final String CREDENTIALS_FORMAT = "%s:%s";
    private static final String BASIC_AUTH = "Basic %s";
    private static final String QUERY_PLACEHOLDER = "%QUERY%";
    private static final String RESULTS_KEY = "hits";

    @Autowired
    private Configuration config;

    @Override
    public List<Document> search(Server server, Algorithm algorithm, Phrase phrase) {
        List<Document> result = new ArrayList<>();
        JSONObject jsonResult = sendQuery(server, algorithm, phrase);
        if (jsonResult.has(RESULTS_KEY)) {
            for (Object docObj : jsonResult.getJSONObject(RESULTS_KEY).getJSONArray(RESULTS_KEY)) {
                if (docObj instanceof JSONObject) {
                    result.add(mapToDocument((JSONObject) docObj));
                }
                if (result.size() >= config.getMaxResults()) {
                    break;
                }
            }
        } else {
            log.error(String.format("Incorrect elastic response. No '%s' field. Response: '%s'", RESULTS_KEY,
                    jsonResult.toString()));
        }
        return result;
    }

    private JSONObject sendQuery(Server server, Algorithm algorithm, Phrase phrase) {
        RestTemplate rest = new RestTemplate();
        rest.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        String body = rest
                .exchange(createUrl(server), HttpMethod.POST,
                        new HttpEntity<String>(createBody(algorithm, phrase), createHeaders(server)), String.class)
                .getBody();
        return new JSONObject(new JSONTokener(body));
    }

    private String createUrl(Server server) {
        return String.format(SEARCH_URL_FORMAT, server.getAddress(), server.getIndex());
    }

    @SuppressWarnings("serial")
    private HttpHeaders createHeaders(final Server server) {
        return new HttpHeaders() {
            {
                String auth = String.format(CREDENTIALS_FORMAT, server.getUsername(), server.getPassword());
                byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
                String authHeader = String.format(BASIC_AUTH, new String(encodedAuth));
                set("Authorization", authHeader);
                setContentType(MediaType.APPLICATION_JSON);
            }
        };
    }

    private String createBody(Algorithm algorithm, Phrase phrase) {
        String text = phrase.getText();
        return algorithm.getParameters().replaceAll(QUERY_PLACEHOLDER, text);
    }

    private Document mapToDocument(JSONObject json) {
        Document document = new Document();
        document.setElasticId(json.getString("_id"));
        JSONObject docSource = json.getJSONObject("_source");
        document.setName(docSource.getString("displayName"));
        // it's too long to keep it
        docSource.remove("fullDesc");
        document.setMetadata(docSource.toString());
        return document;
    }
}
