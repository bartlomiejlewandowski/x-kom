package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@GvNIXJpaBatch(entity = WeightedPhrase.class)
public class WeightedPhraseBatchService {

    @Transactional
    public void create(List<WeightedPhrase> weightedPhrases) {
        for (WeightedPhrase weightedphrase : weightedPhrases) {
            storeWeightedPhrase(weightedphrase);
        }
    }

    private WeightedPhrase storeWeightedPhrase(WeightedPhrase phrase) {
        List<WeightedPhrase> phrases = WeightedPhrase
                .findWeightedPhrasesByPhraseAndPhraseGroup(phrase.getPhrase(), phrase.getPhraseGroup()).getResultList();
        if (phrases.isEmpty()) {
            phrase.persist();
            return phrase;
        } else {
            WeightedPhrase oldPhrase = phrases.get(0);
            oldPhrase.setWeight(phrase.getWeight());
            return oldPhrase.merge();
        }
    }

    @Transactional
    public int deleteAll() {
        List<WeightedPhrase> phrases = WeightedPhrase.findAllWeightedPhrases();
        delete(phrases);
        return phrases.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<WeightedPhrase> phrases = WeightedPhrase.findWeightedPhrasesByIds(ids).getResultList();
        delete(phrases);
        return phrases.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public List<WeightedPhrase> update(List<WeightedPhrase> phrases) {
        List<WeightedPhrase> merged = new ArrayList<>();
        for (WeightedPhrase phrase : phrases) {
            merged.add(storeWeightedPhrase(phrase));
        }
        return merged;
    }

    @Transactional
    public void delete(List<WeightedPhrase> phrases) {
        for (WeightedPhrase phrase : phrases) {
            phrase.remove();
        }
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<WeightedPhrase> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }
}
