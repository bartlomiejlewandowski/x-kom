package com.findwise.xkom.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Documented
@Constraint(validatedBy = ProductFileValidator.class)
public @interface ProductFile {

    String[] contentTypes();

    String message() default "Invalid product file";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}