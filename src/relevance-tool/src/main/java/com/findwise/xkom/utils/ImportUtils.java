package com.findwise.xkom.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.web.multipart.MultipartFile;

abstract class ImportUtils {

    private static final Logger log = Logger.getLogger(ImportUtils.class);

    static boolean hasValidContent(int columnsCount, MultipartFile file, boolean notEmpty) {
        try {
            List<CSVRecord> records = getRecords(file);
            if (records.isEmpty()) {
                log.warn("No records...");
                return false;
            }
            for (CSVRecord record : records) {
                if (record.size() < columnsCount) {
                    log.warn(String.format("Wrong colums number (%d)", record.size()));
                    return false;
                }
                if (notEmpty) {
                    for (int i = 0; i < columnsCount; ++i) {
                        if (record.get(i).isEmpty()) {
                            log.warn(String.format("Column %d may not be empty", i));
                            return false;
                        }
                    }
                }
            }
        } catch (IOException e) {
            log.warn(e);
            return false;
        }
        return true;
    }

    static List<CSVRecord> getRecords(MultipartFile file) throws IOException {
        String fileContent = IOUtils.toString(file.getInputStream(), detectEncoding(file.getInputStream()));
        return CSVParser.parse(fileContent, CSVFormat.EXCEL.withDelimiter(';')).getRecords();
    }

    static String detectEncoding(InputStream is) {
        byte[] buf = new byte[4096];
        try {
            UniversalDetector detector = new UniversalDetector(null);
            int nread;
            while ((nread = is.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
            detector.dataEnd();
            String encoding = detector.getDetectedCharset();
            if (encoding == null) {
                log.warn("No charset detected");
                return Charset.defaultCharset().name();
            }
            return encoding.startsWith("UTF") ? encoding : "ISO8859_2";
        } catch (IOException e) {
            log.error("Error when detecting charset", e);
            return Charset.defaultCharset().name();
        } finally {
            try {
                is.reset();
            } catch (IOException e) {
                log.error("Error when reseting file input stream", e);
            }
        }
    }
}
