package com.findwise.xkom.elastic;

import java.util.List;

import com.findwise.xkom.model.Algorithm;
import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Server;

/**
 * Elastic proxy interface
 * 
 * @author edward.miedzinski
 *
 */
public interface ElasticProxyService {

    /**
     * Send search query to Elasticsearch
     * 
     * @param server
     * @param algorithm
     * @param phrase
     * @return list of results
     */
    List<Document> search(Server server, Algorithm algorithm, Phrase phrase);

}
