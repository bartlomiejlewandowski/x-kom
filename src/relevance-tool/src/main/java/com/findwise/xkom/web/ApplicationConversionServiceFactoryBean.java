package com.findwise.xkom.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

import com.findwise.xkom.model.Algorithm;
import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Server;
import com.findwise.xkom.model.Trial;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.model.WeightedPhrase;

/**
 * A central place to register application converters and formatters.
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

    @Override
    protected void installFormatters(FormatterRegistry registry) {
        super.installFormatters(registry);
        // Register application converters and formatters
    }

    public Converter<Phrase, String> getPhraseToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.Phrase, java.lang.String>() {
            public String convert(Phrase phrase) {
                return phrase.getText();
            }
        };
    }

    public Converter<WebUser, String> getWebUserToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.WebUser, java.lang.String>() {
            public String convert(WebUser webUser) {
                return webUser.getUsername();
            }
        };
    }

    public Converter<Algorithm, String> getAlgorithmToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.Algorithm, java.lang.String>() {
            public String convert(Algorithm algorithm) {
                return algorithm.getName();
            }
        };
    }

    public Converter<Document, String> getDocumentToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.Document, java.lang.String>() {
            public String convert(Document document) {
                return new StringBuilder().append(document.getName()).append(" (").append(document.getElasticId())
                        .append(")").toString();
            }
        };
    }

    public Converter<GroupTrial, String> getGroupTrialToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.GroupTrial, java.lang.String>() {
            public String convert(GroupTrial groupTrial) {
                return groupTrial.getName();
            }
        };
    }

    public Converter<Trial, String> getTrialToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.Trial, java.lang.String>() {
            public String convert(Trial trial) {
                return trial.getName();
            }
        };
    }

    public Converter<Server, String> getServerToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.Server, java.lang.String>() {
            public String convert(Server server) {
                return new StringBuilder().append(server.getAddress()).append('/').append(server.getIndex()).toString();
            }
        };
    }

    public Converter<WeightedPhrase, String> getWeightedPhraseToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.findwise.xkom.model.WeightedPhrase, java.lang.String>() {
            public String convert(WeightedPhrase weightedPhrase) {
                return new StringBuilder().append(weightedPhrase.getPhrase().getText()).append(" (")
                        .append(weightedPhrase.getWeight()).append(")").toString();
            }
        };
    }
}
