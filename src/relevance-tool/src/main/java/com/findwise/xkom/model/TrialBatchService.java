package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.cron.SchedulerService;
import com.findwise.xkom.utils.LoginUtils;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@GvNIXJpaBatch(entity = Trial.class)
public class TrialBatchService {

    @Autowired
    private SchedulerService scheduler;

    @Transactional
    public int deleteAll() {
        List<Trial> trials = Trial.findAllTrialsByLoggedUser();
        delete(trials);
        return trials.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<Trial> trials = Trial.findTrialsByIds(ids).getResultList();
        delete(trials);
        return trials.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<Trial> trials) {
        for (Trial trial : trials) {
            trial.setWebUser(LoginUtils.getCurrentUser());
            trial.persist();
            scheduler.scheduleTrial(trial);
        }
    }

    @Transactional
    public List<Trial> update(List<Trial> trials) {
        List<Trial> merged = new ArrayList<Trial>();
        for (Trial trial : trials) {
            if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
                merged.add(trial.merge());
                scheduler.scheduleTrial(trial);
            }
        }
        return merged;
    }

    public List<Trial> findByValues(Map<String, Object> propertyValues) {

        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();

            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<Trial> entity = new PathBuilder<Trial>(Trial.class, "entity");
            if (!LoginUtils.isAdmin()) {
                baseFilterPredicate.and(entity.get("webUser").eq(LoginUtils.getCurrentUser()));
            }

            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }

            // Create a query with filter
            JPAQuery query = new JPAQuery(Trial.entityManager());
            query = query.from(entity);

            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }

        // no filter: return all elements
        return Trial.findTrialsByWebUser(LoginUtils.getCurrentUser()).getResultList();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<Trial> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

    @Transactional
    public void delete(List<Trial> trials) {
        for (Trial trial : trials) {
            if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
                scheduler.unscheduleJob(trial.getId().toString(), SchedulerService.TRIAL_GROUP);
                trial.remove();
            }
        }
    }
}
