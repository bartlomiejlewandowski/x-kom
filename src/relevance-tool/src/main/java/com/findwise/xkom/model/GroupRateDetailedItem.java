package com.findwise.xkom.model;

import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString(excludeFields = { "groupRateItem" })
@RooJpaActiveRecord(finders = { "findGroupRateDetailedItemsByGroupRateItem", "findGroupRateDetailedItemsByOrdinal",
        "findGroupRateDetailedItemsByOrdinalAndGroupRateItem" })
public class GroupRateDetailedItem {

    /**
     */
    @Value("0")
    private int ordinal;

    /**
     */
    @ManyToOne
    private WeightedPhrase phrase;

    /**
     */
    @ManyToOne
    private Document document;

    /**
     */
    @Value("0")
    private int rateValue;

    /**
     */
    @ManyToOne
    private Rate rate;

    /**
     */
    @ManyToOne
    private GroupRateItem groupRateItem;
}
