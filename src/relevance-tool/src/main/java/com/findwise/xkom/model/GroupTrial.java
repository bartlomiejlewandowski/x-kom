package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.validator.IsCronExpression;

@RooJavaBean
@RooToString(excludeFields = { "webUser" })
@RooJpaActiveRecord(finders = { "findGroupTrialsByWebUser", "findGroupTrialsByPhraseGroupAndWebUser",
        "findGroupTrialsByNameLikeAndWebUser", "findGroupTrialsByNameLikeAndPhraseGroupAndWebUser",
        "findGroupTrialsByPhraseGroup", "findGroupTrialsByNameLike", "findGroupTrialsByNameLikeAndPhraseGroup" })
public class GroupTrial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @NotEmpty
    private String name;

    /**
     */
    @NotNull
    @ManyToOne
    private PhraseGroup phraseGroup;

    /**
     */
    @NotNull
    @ManyToOne
    private Server server;

    /**
     */
    @NotNull
    @ManyToOne
    private Algorithm algorithm;

    /**
     */
    @IsCronExpression
    private String cron;

    /**
     */
    @ManyToOne
    private WebUser webUser;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "trial")
    private Set<GroupTrialRate> rates = new HashSet<GroupTrialRate>();

    public static List<GroupTrial> findAllGroupTrialsByLoggedUser() {
        boolean isAdmin = LoginUtils.isAdmin();
        String condition = isAdmin ? "" : " WHERE o.webUser = :webUser";
        TypedQuery<GroupTrial> query = entityManager().createQuery("SELECT o FROM GroupTrial o" + condition,
                GroupTrial.class);
        if (!isAdmin) {
            query.setParameter("webUser", LoginUtils.getCurrentUser());
        }
        return query.getResultList();
    }

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            GroupTrial attached = GroupTrial.findGroupTrial(this.id);
            this.entityManager.remove(attached);
        }
    }

    /** Custom finders **/
    public static TypedQuery<GroupTrial> findGroupTrialsByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = GroupTrial.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM GroupTrial AS o WHERE o.id IN (:ids)");
        TypedQuery<GroupTrial> q = em.createQuery(queryBuilder.toString(), GroupTrial.class);
        q.setParameter("ids", ids);
        return q;
    }
}
