package com.findwise.xkom.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString(excludeFields = { "phrase", "phraseGroup" })
@RooJpaActiveRecord(finders = { "findWeightedPhrasesByPhraseGroup", "findWeightedPhrasesByPhrase",
        "findWeightedPhrasesByPhraseAndPhraseGroup", "findWeightedPhrasesByPhraseGroupAndWeight",
        "findWeightedPhrasesByWeight" })
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "phrase", "phrase_group" }) })
public class WeightedPhrase {

    /**
     */
    @NotNull
    @ManyToOne
    private Phrase phrase;

    /**
     */
    @NotNull
    @ManyToOne
    private PhraseGroup phraseGroup;

    /**
     */
    @NotNull
    @Value("0.5")
    @Range(min = 0, max = 1)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return this.weight.setScale(2, BigDecimal.ROUND_DOWN);
    }

    /** Custom finders **/
    public static TypedQuery<WeightedPhrase> findWeightedPhrasesByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = Phrase.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM WeightedPhrase AS o WHERE o.id IN (:ids)");
        TypedQuery<WeightedPhrase> q = em.createQuery(queryBuilder.toString(), WeightedPhrase.class);
        q.setParameter("ids", ids);
        return q;
    }
}
