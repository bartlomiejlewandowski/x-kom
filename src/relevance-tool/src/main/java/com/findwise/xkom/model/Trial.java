package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.validator.IsCronExpression;

@RooJavaBean
@RooToString(excludeFields = { "webUser", "rates" })
@RooJpaActiveRecord(finders = { "findTrialsByWebUser", "findTrialsByPhraseAndWebUser", "findTrialsByNameLikeAndWebUser",
        "findTrialsByNameLikeAndPhraseAndWebUser", "findTrialsByPhrase", "findTrialsByNameLike",
        "findTrialsByNameLikeAndPhrase" })
public class Trial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @NotEmpty
    private String name;

    /**
     */
    @NotNull
    @ManyToOne
    private Phrase phrase;

    /**
     */
    @NotNull
    @ManyToOne
    private Server server;

    /**
     */
    @NotNull
    @ManyToOne
    private Algorithm algorithm;

    /**
     */
    @IsCronExpression
    private String cron;

    /**
     */
    @ManyToOne
    private WebUser webUser;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "trial")
    private Set<TrialRate> rates = new HashSet<TrialRate>();

    public static List<Trial> findAllTrialsByLoggedUser() {
        boolean isAdmin = LoginUtils.isAdmin();
        String condition = isAdmin ? "" : " WHERE o.webUser = :webUser";
        TypedQuery<Trial> query = entityManager().createQuery("SELECT o FROM Trial o" + condition, Trial.class);
        if (!isAdmin) {
            query.setParameter("webUser", LoginUtils.getCurrentUser());
        }
        return query.getResultList();
    }

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Trial attached = Trial.findTrial(this.id);
            this.entityManager.remove(attached);
        }
    }

    /** Custom finders **/
    public static TypedQuery<Trial> findTrialsByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = Trial.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Trial AS o WHERE o.id IN (:ids)");
        TypedQuery<Trial> q = em.createQuery(queryBuilder.toString(), Trial.class);
        q.setParameter("ids", ids);
        return q;
    }
}
