package com.findwise.xkom.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findServersByAddress" })
public class Server {

    /**
     */
    @NotNull
    @NotEmpty
    @URL
    private String address;

    /**
     */
    @NotNull
    @NotEmpty
    private String username;

    /**
     */
    @NotNull
    @NotEmpty
    private String password;

    /**
     */
    @NotNull
    @NotEmpty
    private String index;
}
