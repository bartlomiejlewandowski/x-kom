package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.UniqueConstraint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString(excludeFields = { "rateItems", "groupRateItems" })
@RooJpaActiveRecord(finders = { "findRatesByDocumentAndPhrase", "findRatesByRateValue", "findRatesByDocument",
        "findRatesByDocumentAndRateValue", "findRatesByPhrase", "findRatesByPhraseAndRateValue" })
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "phrase", "document" }) })
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @ManyToOne
    private Phrase phrase;

    /**
     */
    @ManyToOne
    private Document document;

    /**
     */
    @Value("0")
    private int rateValue;

    /**
     */
    @OneToMany(mappedBy = "rate")
    private Set<RateItem> rateItems = new HashSet<RateItem>();

    /**
     */
    @OneToMany(mappedBy = "rate")
    private Set<GroupRateDetailedItem> groupRateItems = new HashSet<GroupRateDetailedItem>();

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Rate attached = Rate.findRate(this.id);
            this.entityManager.remove(attached);
        }
    }

    /** Custom finders **/
    public static TypedQuery<Rate> findRatesByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = Rate.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Rate AS o WHERE o.id IN (:ids)");
        TypedQuery<Rate> q = em.createQuery(queryBuilder.toString(), Rate.class);
        q.setParameter("ids", ids);
        return q;
    }
}
