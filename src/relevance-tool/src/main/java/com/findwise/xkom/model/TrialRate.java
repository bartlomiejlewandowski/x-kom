package com.findwise.xkom.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.utils.QueryUtils;
import com.google.common.collect.Lists;

@RooJavaBean
@RooToString(excludeFields = { "trial", "rates", "webUser" })
@RooJpaActiveRecord(finders = { "findTrialRatesByWebUser", "findTrialRatesByTrialAndWebUser", "findTrialRatesByTrial" })
public class TrialRate implements Comparable<TrialRate> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date runDate;

    /**
     */
    @NotNull
    @ManyToOne
    private Trial trial;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "trialRate")
    private Set<RateItem> rates = new HashSet<RateItem>();

    /**
     */
    @NotNull
    @ManyToOne
    private WebUser webUser;

    /**
     */
    private BigDecimal rateValue;

    @Override
    public int compareTo(TrialRate o) {
        return runDate.compareTo(o.runDate);
    }

    public static List<TrialRate> findAllTrialRates() {
        boolean isAdmin = LoginUtils.isAdmin();
        String condition = isAdmin ? "" : " WHERE o.webUser = :webUser";
        TypedQuery<TrialRate> query = entityManager().createQuery("SELECT o FROM TrialRate o" + condition,
                TrialRate.class);
        if (!isAdmin) {
            query.setParameter("webUser", LoginUtils.getCurrentUser());
        }
        return query.getResultList();
    }

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            TrialRate attached = TrialRate.findTrialRate(this.id);
            this.entityManager.remove(attached);
        }
    }

    /** Custom finders **/
    public static TypedQuery<TrialRate> findTrialRatesByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = TrialRate.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM TrialRate AS o WHERE o.id IN (:ids)");
        TypedQuery<TrialRate> q = em.createQuery(queryBuilder.toString(), TrialRate.class);
        q.setParameter("ids", ids);
        return q;
    }

    public static TypedQuery<TrialRate> findTrialRatesByTrialsAndRunDateBetween(List<Trial> trials, Date minRunDate,
            Date maxRunDate, String sortFieldName, String sortOrder) {
        if (trials == null || trials.isEmpty())
            throw new IllegalArgumentException("The trial argument is required");
        if (minRunDate == null)
            throw new IllegalArgumentException("The minRunDate argument is required");
        if (maxRunDate == null)
            throw new IllegalArgumentException("The maxRunDate argument is required");
        EntityManager em = TrialRate.entityManager();
        StringBuilder queryBuilder = new StringBuilder(
                "SELECT o FROM TrialRate AS o WHERE o.trial IN (:trials) AND o.runDate BETWEEN :minRunDate AND :maxRunDate");
        QueryUtils.appendOrderClause(queryBuilder, fieldNames4OrderClauseFilter, sortFieldName, sortOrder);
        TypedQuery<TrialRate> q = em.createQuery(queryBuilder.toString(), TrialRate.class);
        q.setParameter("trials", Lists.newArrayList(trials));
        q.setParameter("minRunDate", minRunDate);
        q.setParameter("maxRunDate", maxRunDate);
        return q;
    }

    public static TypedQuery<TrialRate> findTrialRatesByTrialsAndRunDateBetweenAndWebUser(List<Trial> trials,
            Date minRunDate, Date maxRunDate, WebUser webUser, String sortFieldName, String sortOrder) {
        if (trials == null || trials.isEmpty())
            throw new IllegalArgumentException("The trial argument is required");
        if (minRunDate == null)
            throw new IllegalArgumentException("The minRunDate argument is required");
        if (maxRunDate == null)
            throw new IllegalArgumentException("The maxRunDate argument is required");
        if (webUser == null)
            throw new IllegalArgumentException("The webUser argument is required");
        EntityManager em = TrialRate.entityManager();
        StringBuilder queryBuilder = new StringBuilder(
                "SELECT o FROM TrialRate AS o WHERE o.trial IN (:trials) AND o.runDate BETWEEN :minRunDate AND :maxRunDate  AND o.webUser = :webUser");
        QueryUtils.appendOrderClause(queryBuilder, fieldNames4OrderClauseFilter, sortFieldName, sortOrder);
        TypedQuery<TrialRate> q = em.createQuery(queryBuilder.toString(), TrialRate.class);
        q.setParameter("trials", Lists.newArrayList(trials));
        q.setParameter("minRunDate", minRunDate);
        q.setParameter("maxRunDate", maxRunDate);
        q.setParameter("webUser", webUser);
        return q;
    }

    public static Long countFindTrialRatesByTrialsAndRunDateBetween(List<Trial> trials, Date minRunDate,
            Date maxRunDate) {
        if (trials == null || trials.isEmpty())
            throw new IllegalArgumentException("The trial argument is required");
        if (minRunDate == null)
            throw new IllegalArgumentException("The minRunDate argument is required");
        if (maxRunDate == null)
            throw new IllegalArgumentException("The maxRunDate argument is required");
        EntityManager em = TrialRate.entityManager();
        TypedQuery<Long> q = em.createQuery(
                "SELECT COUNT(o) FROM TrialRate AS o WHERE o.trial IN (:trials) AND o.runDate BETWEEN :minRunDate AND :maxRunDate",
                Long.class);
        q.setParameter("trials", Lists.newArrayList(trials));
        q.setParameter("minRunDate", minRunDate);
        q.setParameter("maxRunDate", maxRunDate);
        return ((Long) q.getSingleResult());
    }

    public static Long countFindTrialRatesByTrialsAndRunDateBetweenAndWebUser(List<Trial> trials, Date minRunDate,
            Date maxRunDate, WebUser webUser) {
        if (trials == null || trials.isEmpty())
            throw new IllegalArgumentException("The trial argument is required");
        if (minRunDate == null)
            throw new IllegalArgumentException("The minRunDate argument is required");
        if (maxRunDate == null)
            throw new IllegalArgumentException("The maxRunDate argument is required");
        if (webUser == null)
            throw new IllegalArgumentException("The webUser argument is required");
        EntityManager em = TrialRate.entityManager();
        TypedQuery<Long> q = em.createQuery(
                "SELECT COUNT(o) FROM TrialRate AS o WHERE o.trial IN (:trials) AND o.runDate BETWEEN :minRunDate AND :maxRunDate  AND o.webUser = :webUser",
                Long.class);
        q.setParameter("trials", Lists.newArrayList(trials));
        q.setParameter("minRunDate", minRunDate);
        q.setParameter("maxRunDate", maxRunDate);
        q.setParameter("webUser", webUser);
        return ((Long) q.getSingleResult());
    }
}
