package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.services.RateService;
import com.google.common.collect.Lists;

@Service
@GvNIXJpaBatch(entity = RateItem.class)
public class RateItemBatchService {

    @Autowired
    private RateService rateService;

    @Transactional
    public List<RateItem> update(List<RateItem> rateitems) {
        List<RateItem> merged = new ArrayList<RateItem>();
        List<Rate> ratesToUpdate = Lists.newArrayList();
        for (RateItem item : rateitems) {
            RateItem oldItem = RateItem.findRateItem(item.getId());
            item.setOrdinal(oldItem.getOrdinal());
            item.setPhrase(oldItem.getPhrase());
            item.setDocument(oldItem.getDocument());
            item.setRate(oldItem.getRate());
            item.setTrialRate(oldItem.getTrialRate());
            ratesToUpdate.add(item.getRate());
            item.getRate().setRateValue(item.getRateValue());
            merged.add(item.merge());
        }
        rateService.updateRates(ratesToUpdate);
        return merged;
    }
}
