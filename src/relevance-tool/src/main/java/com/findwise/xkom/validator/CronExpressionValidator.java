package com.findwise.xkom.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.quartz.CronExpression;

public class CronExpressionValidator implements ConstraintValidator<IsCronExpression, Object> {

    @Override
    public void initialize(IsCronExpression constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return value.toString().isEmpty() || CronExpression.isValidExpression(value.toString());
    }
}
