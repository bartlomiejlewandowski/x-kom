package com.findwise.xkom.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.findwise.xkom.config.Configuration;
import com.findwise.xkom.model.Setting;
import com.findwise.xkom.model.SettingBatchService;

@RequestMapping("/settings")
@Controller
@RooWebScaffold(path = "settings", formBackingObject = Setting.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = SettingBatchService.class)
@GvNIXDatatables(ajax = true)
public class SettingController {

    @Autowired
    private Configuration config;

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Setting setting, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, setting);
            return "settings/update";
        }
        uiModel.asMap().clear();
        setting.merge();
        config.reloadConfig();
        return "redirect:/settings/" + encodeUrlPathSegment(setting.getId().toString(), httpServletRequest);
    }
}
