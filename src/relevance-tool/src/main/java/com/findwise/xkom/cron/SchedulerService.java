package com.findwise.xkom.cron;

import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.Trial;

/**
 * Scheduler interface
 * 
 * @author edward.miedzinski
 *
 */
public interface SchedulerService {

    public static final String TRIAL_GROUP = "trial";
    public static final String GROUP_TRIAL_GROUP = "groupTrial";

    /**
     * Schedule all existing trials
     */
    public void init();

    /**
     * Shutdown scheduler
     */
    public void destroy();

    /**
     * Schedule single trial
     * 
     * @param trial
     */
    public void scheduleTrial(Trial trial);

    /**
     * Schedule single group trial
     * 
     * @param trial
     */
    public void scheduleGroupTrial(GroupTrial trial);

    /**
     * Unschedule single job
     * 
     * @param jobName
     *            name of job
     * @param groupName
     *            name of group
     */
    public void unscheduleJob(String jobName, String groupName);

    /**
     * Unschedule the whole group of jobs
     * 
     * @param groupName
     *            name of group
     */
    public void unscheduleJobGroup(String groupName);
}
