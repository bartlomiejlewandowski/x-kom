package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;

@RooJavaBean
@RooToString(excludeFields = { "phrases", "groupTrials", "webUser" })
@RooJpaActiveRecord(finders = { "findPhraseGroupsByName", "findPhraseGroupsByWebUser",
        "findPhraseGroupsByNameEqualsAndWebUser" })
public class PhraseGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String name;

    /**
     */
    @ManyToOne
    private WebUser webUser;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "phraseGroup")
    private Set<WeightedPhrase> phrases = new HashSet<WeightedPhrase>();

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "phraseGroup")
    private Set<GroupTrial> groupTrials = new HashSet<GroupTrial>();

    public static List<PhraseGroup> findAllPhraseGroups() {
        boolean isAdmin = LoginUtils.isAdmin();
        String condition = isAdmin ? "" : " WHERE o.webUser = :webUser";
        TypedQuery<PhraseGroup> query = entityManager().createQuery("SELECT o FROM PhraseGroup o" + condition,
                PhraseGroup.class);
        if (!isAdmin) {
            query.setParameter("webUser", LoginUtils.getCurrentUser());
        }
        return query.getResultList();
    }

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            PhraseGroup attached = PhraseGroup.findPhraseGroup(this.id);
            this.entityManager.remove(attached);
        }
    }

    public PhraseGroup update() {
        PhraseGroup oldPhraseGroup = PhraseGroup.findPhraseGroup(getId());
        if (LoginUtils.isAdminOrLoggedIn(oldPhraseGroup.getWebUser())
                && PhraseGroup.countFindPhraseGroupsByName(getName()) == 0) {
            setPhrases(oldPhraseGroup.getPhrases());
            setGroupTrials(oldPhraseGroup.getGroupTrials());
            merge();
        }
        return this;
    }

    /** Custom finders **/
    public static TypedQuery<PhraseGroup> findPhraseGroupsByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = PhraseGroup.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM PhraseGroup AS o WHERE o.id IN (:ids)");
        TypedQuery<PhraseGroup> q = em.createQuery(queryBuilder.toString(), PhraseGroup.class);
        q.setParameter("ids", ids);
        return q;
    }
}
