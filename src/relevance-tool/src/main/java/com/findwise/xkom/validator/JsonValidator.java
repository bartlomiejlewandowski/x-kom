package com.findwise.xkom.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonValidator implements ConstraintValidator<IsJson, Object> {

    @Override
    public void initialize(IsJson constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String json = String.class.cast(value);
        try {
            new JSONParser().parse(json);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
