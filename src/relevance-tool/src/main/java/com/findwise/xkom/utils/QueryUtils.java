package com.findwise.xkom.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

public class QueryUtils {

    public static <T> Pair<TypedQuery<T>, Long> getResultInfo(EntityManager em, String objectName,
            Map<String, Object> filters, String[] searchFields, String searchText, List<String> orderClauseFilter,
            String sortFieldName, String sortOrder, Class<T> clazz) {
        String selectClause = String.format("SELECT o FROM %s AS o ", objectName);
        String countClause = String.format("SELECT COUNT(o) FROM %s AS o ", objectName);
        List<String> searchConstraints = new ArrayList<>();
        for (String searchField : searchFields) {
            searchConstraints.add(String.format("LOWER(o.%s) LIKE LOWER(:search)", searchField));
        }
        String searchQuery = String.format("(%s)", StringUtils.join(searchConstraints, " OR "));
        List<String> filterConstraints = new ArrayList<>();
        for (String filterField : filters.keySet()) {
            filterConstraints.add(String.format("o.%s = :%s", filterField, filterField));
        }
        filterConstraints.add(searchQuery);
        String whereCause = String.format("WHERE %s", StringUtils.join(filterConstraints, " AND "));
        StringBuilder queryBuilder = new StringBuilder(selectClause).append(whereCause);
        StringBuilder countBuilder = new StringBuilder(countClause).append(whereCause);
        appendOrderClause(queryBuilder, orderClauseFilter, sortFieldName, sortOrder);
        filters.put("search", String.format("%%%s%%", searchText));
        TypedQuery<T> query = setParameters(em.createQuery(queryBuilder.toString(), clazz), filters);
        Long count = setParameters(em.createQuery(countBuilder.toString(), Long.class), filters).getSingleResult();
        return Pair.of(query, count);
    }

    public static <T> TypedQuery<T> setParameters(TypedQuery<T> query, Map<String, Object> params) {
        for (Entry<String, Object> param : params.entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }
        return query;
    }

    public static void appendOrderClause(StringBuilder queryBuilder, List<String> orderFields, String sortFieldName,
            String sortOrder) {
        if (orderFields.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                queryBuilder.append(" ").append(sortOrder);
            }
        }
    }

    public static List<Long> convertIds(String[] ids) {
        List<Long> longIds = new ArrayList<>();
        for (String id : ids) {
            longIds.add(Long.parseLong(id));
        }
        return longIds;
    }
}
