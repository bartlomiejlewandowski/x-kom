// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.web;

import com.findwise.xkom.model.GroupRateItem;
import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.GroupTrialRate;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.web.GroupTrialRateController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect GroupTrialRateController_Roo_Controller {
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String GroupTrialRateController.show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("grouptrialrate", GroupTrialRate.findGroupTrialRate(id));
        uiModel.addAttribute("itemId", id);
        return "grouptrialrates/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String GroupTrialRateController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("grouptrialrates", GroupTrialRate.findGroupTrialRateEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) GroupTrialRate.countGroupTrialRates() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("grouptrialrates", GroupTrialRate.findAllGroupTrialRates(sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "grouptrialrates/list";
    }
    
    void GroupTrialRateController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("groupTrialRate_rundate_date_format", "yyyy-MM-dd HH:mm:ss");
    }
    
    void GroupTrialRateController.populateEditForm(Model uiModel, GroupTrialRate groupTrialRate) {
        uiModel.addAttribute("groupTrialRate", groupTrialRate);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("grouprateitems", GroupRateItem.findAllGroupRateItems());
        uiModel.addAttribute("grouptrials", GroupTrial.findAllGroupTrials());
        uiModel.addAttribute("webusers", WebUser.findAllWebUsers());
    }
    
    String GroupTrialRateController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
