package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;

@Service
@GvNIXJpaBatch(entity = Phrase.class)
public class PhraseBatchService {

    @Transactional
    public int deleteAll() {
        List<Phrase> phrases = Phrase.findAllPhrases();
        delete(phrases);
        return phrases.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<Phrase> phrases = Phrase.findPhrasesByIds(ids).getResultList();
        delete(phrases);
        return phrases.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<Phrase> phrases) {
        for (Phrase phrase : phrases) {
            if (Phrase.countFindPhrasesByTextEquals(phrase.getText()) == 0) {
                phrase.setWebUser(LoginUtils.getCurrentUser());
                phrase.persist();
            }
        }
    }

    @Transactional
    public List<Phrase> update(List<Phrase> phrases) {
        List<Phrase> merged = new ArrayList<Phrase>();
        for (Phrase phrase : phrases) {
            if (LoginUtils.isAdminOrLoggedIn(phrase.getWebUser())) {
                merged.add(phrase.merge());
            }
        }
        return merged;
    }

    @Transactional
    public void delete(List<Phrase> phrases) {
        for (Phrase phrase : phrases) {
            if (LoginUtils.isAdminOrLoggedIn(phrase.getWebUser())) {
                phrase.remove();
            }
        }
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<Phrase> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }
}
