// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.model;

import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.model.WebUserBatchService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

privileged aspect WebUserBatchService_Roo_GvNIXJpaBatch {
    
    public Class WebUserBatchService.getEntity() {
        return WebUser.class;
    }
    
    public EntityManager WebUserBatchService.entityManager() {
        return WebUser.entityManager();
    }
    
    @Transactional
    public int WebUserBatchService.deleteAll() {
        return entityManager().createQuery("DELETE FROM WebUser").executeUpdate();
    }
    
    @Transactional
    public int WebUserBatchService.deleteIn(List<Long> ids) {
        Query query = entityManager().createQuery("DELETE FROM WebUser as w WHERE w.id IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public int WebUserBatchService.deleteNotIn(List<Long> ids) {
        Query query = entityManager().createQuery("DELETE FROM WebUser as w WHERE w.id NOT IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public void WebUserBatchService.create(List<WebUser> webUsers) {
        for( WebUser webuser : webUsers) {
            webuser.persist();
        }
    }
    
    @Transactional
    public List<WebUser> WebUserBatchService.update(List<WebUser> webusers) {
        List<WebUser> merged = new ArrayList<WebUser>();
        for( WebUser webuser : webusers) {
            merged.add( webuser.merge() );
        }
        return merged;
    }
    
    public List<WebUser> WebUserBatchService.findByValues(Map<String, Object> propertyValues) {
        
        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();
            
            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<WebUser> entity = new PathBuilder<WebUser>(WebUser.class, "entity");
            
            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }
            
            // Create a query with filter
            JPAQuery query = new JPAQuery(WebUser.entityManager());
            query = query.from(entity);
            
            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }
        
        // no filter: return all elements
        return WebUser.findAllWebUsers();
    }
    
    @Transactional
    public long WebUserBatchService.deleteByValues(Map<String, Object> propertyValues) {
        
        // if there no is a filter
        if (propertyValues == null || propertyValues.isEmpty()) {
            throw new IllegalArgumentException("Missing property values");
        }
        // Prepare a predicate
        BooleanBuilder baseFilterPredicate = new BooleanBuilder();
        
        // Base filter. Using BooleanBuilder, a cascading builder for
        // Predicate expressions
        PathBuilder<WebUser> entity = new PathBuilder<WebUser>(WebUser.class, "entity");
        
        // Build base filter
        for (String key : propertyValues.keySet()) {
            baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
        }
        
        // Create a query with filter
        JPADeleteClause delete = new JPADeleteClause(WebUser.entityManager(),entity);
        
        // execute delete
        return delete.where(baseFilterPredicate).execute();
    }
    
    @Transactional
    public void WebUserBatchService.delete(List<WebUser> webusers) {
        for( WebUser webuser : webusers) {
            webuser.remove();
        }
    }
    
}
