package com.findwise.xkom.cron;

import static org.quartz.JobKey.jobKey;
import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.Trial;

@Service
public class CronSchedulerService implements SchedulerService {

    private static final Logger log = Logger.getLogger(CronSchedulerService.class);

    @Autowired
    private JobFactory jobFactory;
    private Scheduler scheduler;

    @Override
    @PostConstruct
    public void init() {
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.setJobFactory(jobFactory);
            scheduler.start();
            for (Trial trial : Trial.findAllTrials()) {
                scheduleTrial(trial);
            }
            for (GroupTrial trial : GroupTrial.findAllGroupTrials()) {
                scheduleGroupTrial(trial);
            }
        } catch (SchedulerException e) {
            log.error("Error while creating scheduler", e);
        }
    }

    @Override
    public void destroy() {
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            log.error("Error while scheduler shutdown", e);
        }
    }

    @Override
    public void scheduleTrial(Trial trial) {
        scheduleJob(PerformTrialJob.class, trial.getId(), TRIAL_GROUP, trial.getCron());
    }

    @Override
    public void scheduleGroupTrial(GroupTrial trial) {
        scheduleJob(PerformGroupTrialJob.class, trial.getId(), GROUP_TRIAL_GROUP, trial.getCron());
    }

    @Override
    public void unscheduleJob(String jobName, String groupName) {
        try {
            scheduler.deleteJob(jobKey(jobName, groupName));
        } catch (SchedulerException e) {
            log.error("Error while unscheduling job", e);
        }
    }

    @Override
    public void unscheduleJobGroup(String groupName) {
        try {
            scheduler.deleteJobs(new ArrayList<>(scheduler.getJobKeys(jobGroupEquals(groupName))));
        } catch (SchedulerException e) {
            log.error("Error while unscheduling group", e);
        }
    }

    private void scheduleJob(Class<? extends Job> jobClass, Long trialId, String groupName, String cronExpression) {
        String jobName = trialId.toString();
        // unschedule job if exists first
        unscheduleJob(jobName, groupName);
        if (StringUtils.isEmpty(cronExpression)) {
            return;
        }
        JobDetail job = JobBuilder.newJob(jobClass).withIdentity(jobName, groupName).usingJobData("trialId", trialId)
                .build();
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName, groupName)
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
        try {
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            log.error("Error while scheduling job", e);
        }
    }
}
