package com.findwise.xkom.model;

import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.web.multipart.MultipartFile;

import com.findwise.xkom.validator.ProductFile;

@RooJavaBean
public class ProductImport {

    /**
     */
    @NotNull
    @ProductFile(contentTypes = { "text/csv", "application/vnd.ms-excel" })
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
