// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.model;

import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.PhraseGroupBatchService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

privileged aspect PhraseGroupBatchService_Roo_GvNIXJpaBatch {
    
    public Class PhraseGroupBatchService.getEntity() {
        return PhraseGroup.class;
    }
    
    public EntityManager PhraseGroupBatchService.entityManager() {
        return PhraseGroup.entityManager();
    }
    
    public List<PhraseGroup> PhraseGroupBatchService.findByValues(Map<String, Object> propertyValues) {
        
        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();
            
            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<PhraseGroup> entity = new PathBuilder<PhraseGroup>(PhraseGroup.class, "entity");
            
            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }
            
            // Create a query with filter
            JPAQuery query = new JPAQuery(PhraseGroup.entityManager());
            query = query.from(entity);
            
            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }
        
        // no filter: return all elements
        return PhraseGroup.findAllPhraseGroups();
    }
    
}
