package com.findwise.xkom.utils;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.findwise.xkom.model.WebUser;

public class LoginUtils {

    private static final Logger log = Logger.getLogger(LoginUtils.class);

    public static WebUser getCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<WebUser> webUserList = WebUser.findWebUsersByUsernameEquals(username).getResultList();
        if (!webUserList.isEmpty()) {
            return webUserList.get(0);
        }
        log.error("No logged user found");
        return null;
    }

    public static boolean isAdminOrLoggedIn(WebUser user) {
        if (user == null || user.getId() == null) {
            return false;
        }
        WebUser loggedUser = LoginUtils.getCurrentUser();
        if (loggedUser == null || loggedUser.getId() == null) {
            return false;
        }
        return hasAdminRole(loggedUser) || user.getId().equals(loggedUser.getId());
    }

    public static boolean isAdmin() {
        WebUser loggedUser = LoginUtils.getCurrentUser();
        if (loggedUser == null || loggedUser.getId() == null) {
            return false;
        }
        return hasAdminRole(loggedUser);
    }

    private static boolean hasAdminRole(WebUser loggedUser) {
        return loggedUser.getUserRole().getName().equals("ROLE_ADMIN");
    }
}
