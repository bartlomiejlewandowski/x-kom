package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@GvNIXJpaBatch(entity = TrialRate.class)
public class TrialRateBatchService {

    @Transactional
    public int deleteAll() {
        List<TrialRate> rates = TrialRate.findAllTrialRates();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<TrialRate> rates = TrialRate.findTrialRatesByIds(ids).getResultList();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<TrialRate> trialRates) {
        // user can't create trial rate
        return;
    }

    @Transactional
    public List<TrialRate> update(List<TrialRate> trialrates) {
        // user can't edit trial rate
        return new ArrayList<TrialRate>();
    }

    public List<TrialRate> findByValues(Map<String, Object> propertyValues) {

        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();

            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<TrialRate> entity = new PathBuilder<TrialRate>(TrialRate.class, "entity");
            if (!LoginUtils.isAdmin()) {
                baseFilterPredicate.and(entity.get("webUser").eq(LoginUtils.getCurrentUser()));
            }

            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }

            // Create a query with filter
            JPAQuery query = new JPAQuery(TrialRate.entityManager());
            query = query.from(entity);

            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }

        // no filter: return all elements
        return TrialRate.findTrialRatesByWebUser(LoginUtils.getCurrentUser()).getResultList();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<TrialRate> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

    @Transactional
    public void delete(List<TrialRate> trialrates) {
        for (TrialRate trialrate : trialrates) {
            if (LoginUtils.isAdminOrLoggedIn(trialrate.getWebUser())) {
                trialrate.remove();
            }
        }
    }
}
