package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.cron.SchedulerService;
import com.findwise.xkom.utils.LoginUtils;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@GvNIXJpaBatch(entity = GroupTrial.class)
public class GroupTrialBatchService {

    @Autowired
    private SchedulerService scheduler;

    @Transactional
    public int deleteAll() {
        List<GroupTrial> trials = GroupTrial.findAllGroupTrialsByLoggedUser();
        delete(trials);
        return trials.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<GroupTrial> trials = GroupTrial.findGroupTrialsByIds(ids).getResultList();
        delete(trials);
        return trials.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<GroupTrial> groupTrials) {
        for (GroupTrial grouptrial : groupTrials) {
            grouptrial.setWebUser(LoginUtils.getCurrentUser());
            grouptrial.persist();
            scheduler.scheduleGroupTrial(grouptrial);
        }
    }

    @Transactional
    public List<GroupTrial> update(List<GroupTrial> grouptrials) {
        List<GroupTrial> merged = new ArrayList<GroupTrial>();
        for (GroupTrial grouptrial : grouptrials) {
            if (LoginUtils.isAdminOrLoggedIn(grouptrial.getWebUser())) {
                merged.add(grouptrial.merge());
                scheduler.scheduleGroupTrial(grouptrial);
            }
        }
        return merged;
    }

    public List<GroupTrial> findByValues(Map<String, Object> propertyValues) {

        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();

            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<GroupTrial> entity = new PathBuilder<GroupTrial>(GroupTrial.class, "entity");
            if (!LoginUtils.isAdmin()) {
                baseFilterPredicate.and(entity.get("webUser").eq(LoginUtils.getCurrentUser()));
            }

            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }

            // Create a query with filter
            JPAQuery query = new JPAQuery(GroupTrial.entityManager());
            query = query.from(entity);

            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }

        // no filter: return all elements
        return GroupTrial.findGroupTrialsByWebUser(LoginUtils.getCurrentUser()).getResultList();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<GroupTrial> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

    @Transactional
    public void delete(List<GroupTrial> grouptrials) {
        for (GroupTrial grouptrial : grouptrials) {
            if (LoginUtils.isAdminOrLoggedIn(grouptrial.getWebUser())) {
                scheduler.unscheduleJob(grouptrial.getId().toString(), SchedulerService.GROUP_TRIAL_GROUP);
                grouptrial.remove();
            }
        }
    }
}
