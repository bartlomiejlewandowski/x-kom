package com.findwise.xkom.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class DateUtils {

    private static final Logger log = Logger.getLogger(DateUtils.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String dateToString(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static Date stringToDate(String date) {
        try {
            return DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            log.error(String.format("Error when parsing date: %s", date), e);
        }
        return new Date();
    }
}
