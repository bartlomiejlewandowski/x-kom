package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.services.RateService;

@Service
@GvNIXJpaBatch(entity = Rate.class)
public class RateBatchService {

    @Autowired
    private RateService rateService;

    @Transactional
    public List<Rate> update(List<Rate> rates) {
        List<Rate> merged = new ArrayList<Rate>();
        for (Rate rate : rates) {
            Rate oldRate = Rate.findRate(rate.getId());
            rate.setDocument(oldRate.getDocument());
            rate.setPhrase(oldRate.getPhrase());
            merged.add(rate.merge());
        }
        rateService.updateRates(merged);
        return merged;
    }

    @Transactional
    public void create(List<Rate> rates) {
        for (Rate rate : rates) {
            rateService.storeRate(rate);
        }
    }

    @Transactional
    public int deleteAll() {
        List<Rate> rates = Rate.findAllRates();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<Rate> rates = Rate.findRatesByIds(ids).getResultList();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<Rate> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }
}
