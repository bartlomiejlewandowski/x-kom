package com.findwise.xkom.cron;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.elastic.TrialExecutorService;
import com.findwise.xkom.model.Trial;

/**
 * Trial execution job
 * 
 * @author edward.miedzinski
 *
 */
@Component
public class PerformTrialJob implements Job {

    private static final Logger log = Logger.getLogger(PerformTrialJob.class);

    @Autowired
    private TrialExecutorService executor;

    @Override
    @Transactional
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Long trialId = context.getJobDetail().getJobDataMap().getLong("trialId");
        Trial trial = Trial.findTrial(trialId);
        log.info(String.format("Executing trial: '%s'", trial.getName()));
        executor.executeTrial(trial, true);
    }
}
