package com.findwise.xkom.elastic;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findwise.xkom.model.Algorithm;
import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.GroupRateDetailedItem;
import com.findwise.xkom.model.GroupRateItem;
import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.GroupTrialRate;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Rate;
import com.findwise.xkom.model.RateItem;
import com.findwise.xkom.model.Server;
import com.findwise.xkom.model.Trial;
import com.findwise.xkom.model.TrialRate;
import com.findwise.xkom.model.WeightedPhrase;
import com.findwise.xkom.services.RateService;

@Service
public class ElasticTrialExecutorService implements TrialExecutorService {

    @Autowired
    private ElasticProxyService elastic;

    @Autowired
    private RateService rateService;

    public TrialRate executeTrial(Trial trial, boolean persistResults) {
        TrialRate trialRate = new TrialRate();
        Set<RateItem> rates = executePhrase(trial.getServer(), trial.getAlgorithm(), trialRate, trial.getPhrase(),
                persistResults);
        trialRate.setRunDate(new Date());
        trialRate.setTrial(trial);
        trialRate.setWebUser(trial.getWebUser());
        trialRate.setRates(rates);
        trialRate.setRateValue(rateService.calculateRate(rates));
        if (persistResults) {
            try {
                trialRate.persist();
            } catch (Exception e) {
                trialRate.merge();
            }
        }
        return trialRate;
    }

    public GroupTrialRate executeGroupTrial(GroupTrial groupTrial, boolean persistResults) {
        GroupTrialRate groupTrialRate = new GroupTrialRate();
        Set<GroupRateItem> rates = new HashSet<>();
        for (WeightedPhrase weightedPhrase : groupTrial.getPhraseGroup().getPhrases()) {
            rates.add(executeWeightedPhrase(groupTrial.getServer(), groupTrial.getAlgorithm(), groupTrialRate,
                    weightedPhrase, persistResults));
        }
        groupTrialRate.setRunDate(new Date());
        groupTrialRate.setTrial(groupTrial);
        groupTrialRate.setWebUser(groupTrial.getWebUser());
        groupTrialRate.setRates(rates);
        groupTrialRate.setRateValue(rateService.calculateGroupRate(rates));
        if (persistResults) {
            try {
                groupTrialRate.persist();
            } catch (Exception e) {
                groupTrialRate.merge();
            }
        }
        return groupTrialRate;
    }

    private Set<RateItem> executePhrase(Server server, Algorithm algorithm, TrialRate trialRate, Phrase phrase,
            boolean persistResults) {
        List<Document> documents = elastic.search(server, algorithm, phrase);
        Set<RateItem> rates = new HashSet<>();
        int i = 1;
        for (Document document : documents) {
            RateItem rateItem = new RateItem();
            Rate rate = new Rate();
            rate.setPhrase(phrase);
            rate.setDocument(fetchDocument(document, persistResults));
            rateItem.setOrdinal(i++);
            rateItem.setPhrase(rate.getPhrase());
            rateItem.setDocument(rate.getDocument());
            rateItem.setRate(fetchRate(rate, persistResults));
            rateItem.setRateValue(rateItem.getRate().getRateValue());
            rateItem.setTrialRate(trialRate);
            rates.add(rateItem);
        }
        return rates;
    }

    private GroupRateItem executeWeightedPhrase(Server server, Algorithm algorithm, GroupTrialRate trialRate,
            WeightedPhrase phrase, boolean persistResults) {
        List<Document> documents = elastic.search(server, algorithm, phrase.getPhrase());
        GroupRateItem rateItem = new GroupRateItem();
        rateItem.setPhrase(phrase);
        rateItem.setGroupTrialRate(trialRate);
        int i = 1;
        Set<GroupRateDetailedItem> rates = new HashSet<>();
        for (Document document : documents) {
            Rate rate = new Rate();
            rate.setPhrase(phrase.getPhrase());
            rate.setDocument(fetchDocument(document, persistResults));
            GroupRateDetailedItem rateDetailedItem = new GroupRateDetailedItem();
            rateDetailedItem.setOrdinal(i++);
            rateDetailedItem.setDocument(rate.getDocument());
            rateDetailedItem.setPhrase(phrase);
            rateDetailedItem.setRate(fetchRate(rate, persistResults));
            rateDetailedItem.setRateValue(rateDetailedItem.getRate().getRateValue());
            rateDetailedItem.setGroupRateItem(rateItem);
            rates.add(rateDetailedItem);
        }
        rateItem.setRates(rates);
        rateItem.setRateValue(rateService.calculateDetailedGroupRate(rates));
        return rateItem;
    }

    private Document fetchDocument(Document document, boolean persistResults) {
        List<Document> documents = Document.findDocumentsByElasticIdEquals(document.getElasticId()).getResultList();
        if (documents.isEmpty()) {
            if (persistResults) {
                try {
                    document.persist();
                } catch (Exception e) {
                    document.merge();
                }
            }
            return document;
        }
        return documents.get(0);
    }

    private Rate fetchRate(Rate rate, boolean persistResults) {
        List<Rate> rates = Rate.findRatesByDocumentAndPhrase(rate.getDocument(), rate.getPhrase()).getResultList();
        if (rates.isEmpty()) {
            if (persistResults) {
                try {
                    rate.persist();
                } catch (Exception e) {
                    rate.merge();
                }
            }
            return rate;
        }
        return rates.get(0);
    }
}
