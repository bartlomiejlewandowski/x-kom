package com.findwise.xkom.web;

import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.RateItem;
import com.findwise.xkom.model.RateItemBatchService;
import com.findwise.xkom.model.TrialRate;
import com.findwise.xkom.services.RateService;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

@RequestMapping("/rateitems")
@Controller
@RooWebScaffold(path = "rateitems", formBackingObject = RateItem.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, inlineEditing = true)
@GvNIXWebJpaBatch(service = RateItemBatchService.class)
public class RateItemController {

    @Autowired
    private RateService rateService;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid RateItem rateItem, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllRateItems(@DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute RateItem rateItem, HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        TrialRate trialRate = rateItem.getTrialRate();
        String search = Strings.isNullOrEmpty(criterias.getSearch()) ? null : criterias.getSearch();
        TypedQuery<RateItem> query = null;
        Long count = 0l;
        String[] searchFields = new String[] { "document.name", "phrase.text" };
        if (trialRate == null && search == null) {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM RateItem AS o");
            QueryUtils.appendOrderClause(queryBuilder, RateItem.fieldNames4OrderClauseFilter, sortFieldName, sortOrder);
            query = RateItem.entityManager().createQuery(queryBuilder.toString(), RateItem.class);
            count = RateItem.countRateItems();
        } else if (trialRate != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("trialRate", trialRate);
            Pair<TypedQuery<RateItem>, Long> resultInfo = QueryUtils.getResultInfo(RateItem.entityManager(), "RateItem",
                    filters, searchFields, search, RateItem.fieldNames4OrderClauseFilter, sortFieldName, sortOrder,
                    RateItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (trialRate != null && search == null) {
            query = RateItem.findRateItemsByTrialRate(trialRate, sortFieldName, sortOrder);
            count = RateItem.countFindRateItemsByTrialRate(trialRate);
        } else {
            Map<String, Object> filters = Maps.newHashMap();
            Pair<TypedQuery<RateItem>, Long> resultInfo = QueryUtils.getResultInfo(RateItem.entityManager(), "RateItem",
                    filters, searchFields, search, RateItem.fieldNames4OrderClauseFilter, sortFieldName, sortOrder,
                    RateItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }

    public void populateItemForRender(HttpServletRequest request, RateItem rateItem, boolean editing) {
        org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();

        request.setAttribute("rateItem", rateItem);
        request.setAttribute("itemId", conversionService_dtt.convert(rateItem.getId(), String.class));

        if (editing) {
            // spring from:input tag uses BindingResult to locate property
            // editors for each bean
            // property. So, we add a request attribute (required key id
            // BindingResult.MODEL_KEY_PREFIX + object name)
            // with a correctly initialized bindingResult.
            BeanPropertyBindingResult bindindResult = new BeanPropertyBindingResult(rateItem, "rateItem");
            bindindResult.initConversion(conversionService_dtt);
            request.setAttribute(BindingResult.MODEL_KEY_PREFIX + "rateItem", bindindResult);
            // Add date time patterns and enums to populate inputs
            populateEditForm(uiModel, rateItem);
        }

        // Load uiModel attributes into request
        Map<String, Object> modelMap = uiModel.asMap();
        for (String key : modelMap.keySet()) {
            request.setAttribute(key, modelMap.get(key));
        }
        request.setAttribute("rateValues", rateService.populateRateValues());
    }

    @ModelAttribute
    public void populateDatatablesConfig(Model uiModel) {
        uiModel.addAttribute("datatablesHasBatchSupport", true);
        uiModel.addAttribute("datatablesUseAjax", true);
        uiModel.addAttribute("datatablesInlineEditing", true);
        uiModel.addAttribute("datatablesInlineCreating", false);
        uiModel.addAttribute("datatablesSecurityApplied", true);
        uiModel.addAttribute("datatablesStandardMode", true);
        uiModel.addAttribute("finderNameParam", "ajax_find");
    }
}
