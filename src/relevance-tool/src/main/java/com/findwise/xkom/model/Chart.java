package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class Chart<XType, YType> {

    private String title;

    private boolean zoomEnabled;

    private List<Series<XType, YType>> series;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isZoomEnabled() {
        return zoomEnabled;
    }

    public void setZoomEnabled(boolean zoomEnabled) {
        this.zoomEnabled = zoomEnabled;
    }

    public List<Series<XType, YType>> getSeries() {
        return series;
    }

    public void setSeries(List<Series<XType, YType>> series) {
        this.series = series;
    }

    public static class Series<XType, YType> {

        private String name;

        private boolean showInLegend;

        private String type;

        private String xValueType;

        private List<Pair<XType, YType>> points = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isShowInLegend() {
            return showInLegend;
        }

        public void setShowInLegend(boolean showInLegend) {
            this.showInLegend = showInLegend;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getxValueType() {
            return xValueType;
        }

        public void setxValueType(String xValueType) {
            this.xValueType = xValueType;
        }

        public List<Pair<XType, YType>> getPoints() {
            return points;
        }

        public void addPoint(Pair<XType, YType> point) {
            this.points.add(point);
        }
    }
}
