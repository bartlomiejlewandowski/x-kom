package com.findwise.xkom.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString(excludeFields = { "groupTrialRate" })
@RooJpaActiveRecord(finders = { "findGroupRateItemsByGroupTrialRate" })
public class GroupRateItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @ManyToOne
    private WeightedPhrase phrase;

    /**
     */
    private BigDecimal rateValue;

    /**
     */
    @NotNull
    @ManyToOne
    private GroupTrialRate groupTrialRate;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "groupRateItem")
    private Set<GroupRateDetailedItem> rates = new HashSet<GroupRateDetailedItem>();

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            GroupRateItem attached = GroupRateItem.findGroupRateItem(this.id);
            this.entityManager.remove(attached);
        }
    }
}
