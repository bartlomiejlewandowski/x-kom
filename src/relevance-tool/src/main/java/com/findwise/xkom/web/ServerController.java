package com.findwise.xkom.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.findwise.xkom.model.Server;
import com.findwise.xkom.model.ServerBatchService;
import com.findwise.xkom.utils.LoginUtils;

@RequestMapping("/servers")
@Controller
@RooWebScaffold(path = "servers", formBackingObject = Server.class)
@GvNIXWebJpaBatch(service = ServerBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true)
public class ServerController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Server server, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, server);
            return "servers/create";
        }
        if (LoginUtils.isAdmin()) {
            uiModel.asMap().clear();
            server.persist();
        }
        return "redirect:/servers/" + encodeUrlPathSegment(server.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        if (LoginUtils.isAdmin()) {
            populateEditForm(uiModel, new Server());
        }
        return "servers/create";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Server server, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, server);
            return "servers/update";
        }
        if (LoginUtils.isAdmin()) {
            uiModel.asMap().clear();
            server.merge();
        }
        return "redirect:/servers/" + encodeUrlPathSegment(server.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        if (LoginUtils.isAdmin()) {
            populateEditForm(uiModel, Server.findServer(id));
        }
        return "servers/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Server server = Server.findServer(id);
        if (LoginUtils.isAdmin()) {
            server.remove();
            uiModel.asMap().clear();
        }
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/servers";
    }
}
