package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDocumentsByElasticIdEquals" })
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @Column(unique = true)
    private String elasticId;

    /**
     */
    @NotNull
    private String name;

    /**
     */
    @Column(length = 5000)
    private String metadata;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "document")
    private Set<Rate> rates = new HashSet<Rate>();

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Document attached = Document.findDocument(this.id);
            this.entityManager.remove(attached);
        }
    }

    public Document update() {
        Document oldDoc = Document.findDocument(getId());
        setMetadata(oldDoc.getMetadata());
        setRates(oldDoc.getRates());
        merge();
        return this;
    }

    /** Custom finders **/
    public static TypedQuery<Document> findDocumentsByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = Document.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Document AS o WHERE o.id IN (:ids)");
        TypedQuery<Document> q = em.createQuery(queryBuilder.toString(), Document.class);
        q.setParameter("ids", ids);
        return q;
    }
}
