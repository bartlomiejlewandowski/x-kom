package com.findwise.xkom.web;

import com.findwise.xkom.model.Algorithm;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.findwise.xkom.model.AlgorithmBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/algorithms")
@Controller
@RooWebScaffold(path = "algorithms", formBackingObject = Algorithm.class)
@GvNIXWebJpaBatch(service = AlgorithmBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true)
public class AlgorithmController {
}
