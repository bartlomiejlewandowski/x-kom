package com.findwise.xkom.utils;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Sets;

public class RequestUtils {

    public static Set<Pair<String, String>> mergeMultivalue(Map<String, String> params, HttpServletRequest request) {
        Set<Pair<String, String>> result = Sets.newHashSet();
        for (Entry<String, String> paramEntry : params.entrySet()) {
            for (String value : request.getParameterValues(paramEntry.getKey())) {
                result.add(new MutablePair<>(paramEntry.getKey(), value));
            }
        }
        return result;
    }
}
