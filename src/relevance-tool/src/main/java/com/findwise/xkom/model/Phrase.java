package com.findwise.xkom.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;

@RooJavaBean
@RooToString(excludeFields = { "webUser", "phraseGroups", "trials", "rates" })
@RooJpaActiveRecord(finders = { "findPhrasesByTextLike", "findPhrasesByWebUser", "findPhrasesByTextEquals" })
public class Phrase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     */
    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String text;

    /**
     */
    @ManyToOne
    private WebUser webUser;

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "phrase")
    private Set<WeightedPhrase> phraseGroups = new HashSet<WeightedPhrase>();

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "phrase")
    private Set<Trial> trials = new HashSet<Trial>();

    /**
     */
    @OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH }, mappedBy = "phrase")
    private Set<Rate> rates = new HashSet<Rate>();

    public static List<Phrase> findAllPhrases() {
        boolean isAdmin = LoginUtils.isAdmin();
        String condition = isAdmin ? "" : " WHERE o.webUser = :webUser";
        TypedQuery<Phrase> query = entityManager().createQuery("SELECT o FROM Phrase o" + condition, Phrase.class);
        if (!isAdmin) {
            query.setParameter("webUser", LoginUtils.getCurrentUser());
        }
        return query.getResultList();
    }

    @Transactional
    public void remove() {
        if (this.entityManager == null)
            this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Phrase attached = Phrase.findPhrase(this.id);
            this.entityManager.remove(attached);
        }
    }

    /** Custom finders **/
    public static TypedQuery<Phrase> findPhrasesByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty())
            throw new IllegalArgumentException("The ids argument is required");
        EntityManager em = Phrase.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Phrase AS o WHERE o.id IN (:ids)");
        TypedQuery<Phrase> q = em.createQuery(queryBuilder.toString(), Phrase.class);
        q.setParameter("ids", ids);
        return q;
    }
}
