package com.findwise.xkom.web;

import com.findwise.xkom.model.WebUserRole;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.findwise.xkom.model.WebUserRoleBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/webuserroles")
@Controller
@RooWebScaffold(path = "webuserroles", formBackingObject = WebUserRole.class)
@GvNIXWebJpaBatch(service = WebUserRoleBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(inlineEditing = true, ajax = true)
public class WebUserRoleController {
}
