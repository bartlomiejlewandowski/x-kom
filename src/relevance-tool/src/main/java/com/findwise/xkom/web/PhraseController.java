package com.findwise.xkom.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.gvnix.web.datatables.query.SearchResults;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.PhraseBatchService;
import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.PhraseImport;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.model.WeightedPhrase;
import com.findwise.xkom.utils.PhraseImportUtils;
import com.findwise.xkom.utils.LoginUtils;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;

@RequestMapping("/phrases")
@Controller
@RooWebScaffold(path = "phrases", formBackingObject = Phrase.class)
@GvNIXWebJpaBatch(service = PhraseBatchService.class)
@GvNIXWebJQuery
@GvNIXLoupeController
@GvNIXDatatables(inlineEditing = true, ajax = true, detailFields = { "trials", "rates" })
public class PhraseController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Phrase phrase, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, phrase);
            return "phrases/create";
        }
        if (Phrase.countFindPhrasesByTextEquals(phrase.getText()) == 0) {
            phrase.setWebUser(LoginUtils.getCurrentUser());
            uiModel.asMap().clear();
            phrase.persist();
        }
        return "redirect:/phrases";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Phrase phrase, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, phrase);
            return "phrases/update";
        }
        Phrase oldPhrase = Phrase.findPhrase(phrase.getId());
        if (LoginUtils.isAdminOrLoggedIn(oldPhrase.getWebUser())) {
            phrase.setPhraseGroups(oldPhrase.getPhraseGroups());
            phrase.setRates(oldPhrase.getRates());
            phrase.setTrials(oldPhrase.getTrials());
            phrase.merge();
        }
        uiModel.asMap().clear();
        return "redirect:/phrases/" + encodeUrlPathSegment(phrase.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        Phrase phrase = Phrase.findPhrase(id);
        if (LoginUtils.isAdminOrLoggedIn(phrase.getWebUser())) {
            populateEditForm(uiModel, Phrase.findPhrase(id));
        }
        return "phrases/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Phrase phrase = Phrase.findPhrase(id);
        if (LoginUtils.isAdminOrLoggedIn(phrase.getWebUser())) {
            phrase.remove();
        }
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/phrases";
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "Phrase_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");
        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }

    @RequestMapping(params = "my", method = RequestMethod.GET)
    public String findPhrasesByLoggedUser(@RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sortFieldName", required = false) String sortFieldName,
            @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        WebUser webUser = LoginUtils.getCurrentUser();
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("my_phrases", Phrase.findPhrasesByWebUser(webUser, sortFieldName, sortOrder)
                    .setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Phrase.countFindPhrasesByWebUser(webUser) / sizeNo;
            uiModel.addAttribute("maxPages",
                    (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("my_phrases",
                    Phrase.findPhrasesByWebUser(webUser, sortFieldName, sortOrder).getResultList());
        }
        uiModel.asMap().put("datatablesFiltered", true);
        // Add attribute available into view with information about each detail
        // datatables
        Map<String, String> details;
        List<Map<String, String>> detailsInfo = new ArrayList<Map<String, String>>(2);
        details = new HashMap<String, String>();
        // Base path for detail datatables entity (to get detail datatables
        // fragment URL)
        details.put("path", "trials");
        details.put("property", "trials");
        // Property name in detail entity with the relation to master entity
        details.put("mappedBy", "phrase");
        detailsInfo.add(details);
        details = new HashMap<String, String>();
        // Base path for detail datatables entity (to get detail datatables
        // fragment URL)
        details.put("path", "rates");
        details.put("property", "rates");
        // Property name in detail entity with the relation to master entity
        details.put("mappedBy", "phrase");
        detailsInfo.add(details);
        uiModel.addAttribute("detailsInfo", detailsInfo);
        return "phrases/list_my";
    }

    @RequestMapping(headers = "Accept=application/json", params = "filtered", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllPhrasesByLoggedUser(
            @DatatablesParams DatatablesCriterias criterias, @ModelAttribute Phrase phrase,
            HttpServletRequest request) {
        // URL parameters are used as base search filters
        Map<String, Object> baseSearchValuesMap = getPropertyMap(phrase, request);
        if (!LoginUtils.isAdmin()) {
            baseSearchValuesMap.put("webUser", LoginUtils.getCurrentUser());
        }
        setDatatablesBaseFilter(baseSearchValuesMap);
        SearchResults<Phrase> searchResult = datatablesUtilsBean_dtt.findByCriteria(Phrase.class, criterias,
                baseSearchValuesMap);
        // Get datatables required counts
        long totalRecords = searchResult.getTotalCount();
        long recordsFound = searchResult.getResultsCount();
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(searchResult.getResults(),
                pkFieldName, totalRecords, recordsFound, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }

    /**
     * Import
     */
    @RequestMapping(value = "/import", params = "form", produces = "text/html")
    public String importForm(Model uiModel) {
        populateImportForm(uiModel, new PhraseImport());
        return "phrases/import";
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST, consumes = "multipart/form-data", produces = "text/html")
    @Transactional
    public String importPhrases(@Valid PhraseImport phraseImport, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateImportForm(uiModel, phraseImport);
            return "phrases/import";
        }
        Map<String, List<Pair<String, BigDecimal>>> phrasesMap;
        try {
            phrasesMap = PhraseImportUtils.getPhrasesMap(phraseImport.getFile());
        } catch (IOException e) {
            return "phrases/import";
        }
        WebUser webUser = LoginUtils.getCurrentUser();
        for (Entry<String, List<Pair<String, BigDecimal>>> phraseEntry : phrasesMap.entrySet()) {
            String groupName = phraseEntry.getKey();
            PhraseGroup phraseGroup = null;
            if (!PhraseImportUtils.WITHOUT_GROUP.equals(groupName)) {
                List<PhraseGroup> phraseGroups = PhraseGroup.findPhraseGroupsByNameEqualsAndWebUser(groupName, webUser)
                        .getResultList();
                if (phraseGroups.isEmpty()) {
                    phraseGroup = new PhraseGroup();
                    phraseGroup.setName(phraseEntry.getKey());
                    phraseGroup.setWebUser(webUser);
                    phraseGroup.persist();
                } else {
                    phraseGroup = phraseGroups.get(0);
                }
                for (Pair<String, BigDecimal> phraseInfo : phraseEntry.getValue()) {
                    Phrase phrase = storePhrase(webUser, phraseInfo.getKey());
                    TypedQuery<WeightedPhrase> phraseQuery = WeightedPhrase.findWeightedPhrasesByPhraseAndPhraseGroup(phrase, phraseGroup);
                    List<WeightedPhrase> weightedPhrases = phraseQuery.getResultList();
                    if (weightedPhrases.isEmpty()) {
                        WeightedPhrase weightedPhrase = new WeightedPhrase();
                        weightedPhrase.setPhraseGroup(phraseGroup);
                        weightedPhrase.setPhrase(phrase);
                        weightedPhrase.setWeight(phraseInfo.getValue());
                        weightedPhrase.persist();
                    } else {
                        WeightedPhrase weightedPhrase = weightedPhrases.get(0);
                        weightedPhrase.setWeight(phraseInfo.getValue());
                        weightedPhrase.merge();
                    }
                }
            } else {
                // phrases without group
                for (Pair<String, BigDecimal> phraseInfo : phraseEntry.getValue()) {
                    storePhrase(webUser, phraseInfo.getKey());
                }
            }
        }
        return "redirect:/phrases?my";
    }

    private Phrase storePhrase(WebUser webUser, String phraseText) {
        List<Phrase> phrases = Phrase.findPhrasesByTextEquals(phraseText).getResultList();
        if (!phrases.isEmpty()) {
            return phrases.get(0);
        }
        Phrase phrase = new Phrase();
        phrase.setWebUser(webUser);
        phrase.setText(phraseText);
        phrase.persist();
        return phrase;
    }

    void populateImportForm(Model uiModel, PhraseImport phraseImport) {
        uiModel.addAttribute("phraseImport", phraseImport);
    }
}
