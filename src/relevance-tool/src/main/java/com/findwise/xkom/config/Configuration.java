package com.findwise.xkom.config;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import com.findwise.xkom.model.Setting;

@Component
public class Configuration {

    private static final String MAX_RESULTS_KEY = "max_results";
    private static final String WEIGHTS_KEY = "result_weights";

    private static final String NEW_LINE = "\\r?\\n";

    private static final int DEFAULT_MAX_RESULTS = 10;
    private static final double DEFAULT_WEIGHT = 1.;
    private static final int DEFAULT_NUMBER_OF_WEIGHTS = 100;

    private int maxResults;
    private double[] resultWeights;

    @PostConstruct
    public void reloadConfig() {
        setMaxResults(getInt(MAX_RESULTS_KEY, DEFAULT_MAX_RESULTS));
        setResultWeights(getDoubles(WEIGHTS_KEY, DEFAULT_WEIGHT));
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    public double[] getResultWeights() {
        return resultWeights;
    }

    public void setResultWeights(double[] resultWeights) {
        this.resultWeights = resultWeights;
    }

    private int getInt(String key, int defaultValue) {
        String stringValue = getValue(key);
        return NumberUtils.isNumber(stringValue) ? Integer.parseInt(stringValue) : defaultValue;
    }

    private double[] getDoubles(String key, double defaultValue) {
        String stringValue = getValue(key);
        if (StringUtils.isEmpty(stringValue)) {
            double[] defaultWeights = new double[DEFAULT_NUMBER_OF_WEIGHTS];
            Arrays.fill(defaultWeights, DEFAULT_WEIGHT);
            return defaultWeights;
        }
        String[] values = stringValue.split(NEW_LINE);
        double[] weights = new double[values.length];
        for (int i = 0; i < weights.length; ++i) {
            weights[i] = NumberUtils.isNumber(values[i]) ? Double.parseDouble(values[i]) : DEFAULT_WEIGHT;
        }
        return weights;
    }

    private String getValue(String key) {
        return Setting.findSettingsByName(key).getSingleResult().getParamValue();
    }
}
