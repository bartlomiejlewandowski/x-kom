package com.findwise.xkom.web;

import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.GroupRateItem;
import com.findwise.xkom.model.GroupTrialRate;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

@RequestMapping("/grouprateitems")
@Controller
@RooWebScaffold(path = "grouprateitems", formBackingObject = GroupRateItem.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, detailFields = { "rates" })
public class GroupRateItemController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid GroupRateItem groupRateItem, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid GroupRateItem groupRateItem, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllGroupRateItems(
            @DatatablesParams DatatablesCriterias criterias, @ModelAttribute GroupRateItem groupRateItem,
            HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        GroupTrialRate trialRate = groupRateItem.getGroupTrialRate();
        String search = Strings.isNullOrEmpty(criterias.getSearch()) ? null : criterias.getSearch();
        TypedQuery<GroupRateItem> query = null;
        Long count = 0l;
        String[] searchFields = new String[] { "phrase.phrase.text" };
        if (trialRate == null && search == null) {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM GroupRateItem AS o");
            QueryUtils.appendOrderClause(queryBuilder, GroupRateItem.fieldNames4OrderClauseFilter, sortFieldName,
                    sortOrder);
            query = GroupRateItem.entityManager().createQuery(queryBuilder.toString(), GroupRateItem.class);
            count = GroupRateItem.countGroupRateItems();
        } else if (trialRate != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("groupTrialRate", trialRate);
            Pair<TypedQuery<GroupRateItem>, Long> resultInfo = QueryUtils.getResultInfo(GroupRateItem.entityManager(),
                    "GroupRateItem", filters, searchFields, search, GroupRateItem.fieldNames4OrderClauseFilter,
                    sortFieldName, sortOrder, GroupRateItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            Pair<TypedQuery<GroupRateItem>, Long> resultInfo = QueryUtils.getResultInfo(GroupRateItem.entityManager(),
                    "GroupRateItem", filters, searchFields, search, GroupRateItem.fieldNames4OrderClauseFilter,
                    sortFieldName, sortOrder, GroupRateItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else {
            query = GroupRateItem.findGroupRateItemsByGroupTrialRate(trialRate, sortFieldName, sortOrder);
            count = GroupRateItem.countFindGroupRateItemsByGroupTrialRate(trialRate);
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }
}
