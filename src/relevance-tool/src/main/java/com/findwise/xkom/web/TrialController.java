package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.cron.SchedulerService;
import com.findwise.xkom.elastic.TrialExecutorService;
import com.findwise.xkom.model.Phrase;
import com.findwise.xkom.model.Trial;
import com.findwise.xkom.model.TrialBatchService;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;

@RequestMapping("/trials")
@Controller
@RooWebScaffold(path = "trials", formBackingObject = Trial.class)
@GvNIXWebJpaBatch(service = TrialBatchService.class)
@GvNIXWebJQuery
@GvNIXLoupeController
@GvNIXDatatables(ajax = true, detailFields = { "rates" })
public class TrialController {

    @Autowired
    private TrialExecutorService executor;

    @Autowired
    private SchedulerService scheduler;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Trial trial, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, trial);
            return "trials/create";
        }
        trial.setWebUser(LoginUtils.getCurrentUser());
        uiModel.asMap().clear();
        trial.persist();
        scheduler.scheduleTrial(trial);

        return "redirect:/trials/" + encodeUrlPathSegment(trial.getId().toString(), httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Trial trial, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, trial);
            return "trials/update";
        }
        Trial oldTrial = Trial.findTrial(trial.getId());
        if (LoginUtils.isAdminOrLoggedIn(oldTrial.getWebUser())) {
            trial.setWebUser(oldTrial.getWebUser());
            trial.setRates(oldTrial.getRates());
            uiModel.asMap().clear();
            trial.merge();
            scheduler.scheduleTrial(trial);
        }
        return "redirect:/trials/" + encodeUrlPathSegment(trial.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        Trial trial = Trial.findTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            uiModel.addAttribute("trial", trial);
            uiModel.addAttribute("itemId", id);
        }
        return "trials/show";
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        Trial trial = Trial.findTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            populateEditForm(uiModel, trial);
        }
        return "trials/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Trial trial = Trial.findTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            trial.remove();
            scheduler.unscheduleJob(trial.getId().toString(), SchedulerService.TRIAL_GROUP);
        }
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/trials";
    }

    @RequestMapping(value = "/{id}", params = "run", produces = "text/html")
    public String runAction(@PathVariable("id") Long id, Model uiModel, HttpServletRequest httpServletRequest) {
        Trial trial = Trial.findTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            executor.executeTrial(trial, true);
        }
        return "redirect:/trials";
    }

    public void setDatatablesBaseFilter(Map<String, Object> propertyMap) {
        if (!LoginUtils.isAdmin()) {
            propertyMap.put("webUser", LoginUtils.getCurrentUser());
        }
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "Trial_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");
        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllTrials(@DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute Trial trial, HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        WebUser webUser = LoginUtils.getCurrentUser();
        Phrase phrase = trial.getPhrase();

        TypedQuery<Trial> query = null;
        Long count = 0l;
        if (phrase == null && criterias.getSearch().isEmpty()) {
            if (LoginUtils.isAdmin()) {
                StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Trial AS o");
                QueryUtils.appendOrderClause(queryBuilder, Trial.fieldNames4OrderClauseFilter, sortFieldName,
                        sortOrder);
                query = Trial.entityManager().createQuery(queryBuilder.toString(), Trial.class);
                count = Trial.countTrials();
            } else {
                query = Trial.findTrialsByWebUser(webUser, sortFieldName, sortOrder);
                count = Trial.countFindTrialsByWebUser(webUser);
            }
        } else if (phrase != null && !criterias.getSearch().isEmpty()) {
            if (LoginUtils.isAdmin()) {
                query = Trial.findTrialsByNameLikeAndPhrase(criterias.getSearch(), phrase, sortFieldName, sortOrder);
                count = Trial.countFindTrialsByNameLikeAndPhrase(criterias.getSearch(), phrase);
            } else {
                query = Trial.findTrialsByNameLikeAndPhraseAndWebUser(criterias.getSearch(), phrase, webUser,
                        sortFieldName, sortOrder);
                count = Trial.countFindTrialsByNameLikeAndPhraseAndWebUser(criterias.getSearch(), phrase, webUser);
            }
        } else if (phrase != null) {
            if (LoginUtils.isAdmin()) {
                query = Trial.findTrialsByPhrase(phrase, sortFieldName, sortOrder);
                count = Trial.countFindTrialsByPhrase(phrase);
            } else {
                query = Trial.findTrialsByPhraseAndWebUser(phrase, webUser, sortFieldName, sortOrder);
                count = Trial.countFindTrialsByPhraseAndWebUser(phrase, webUser);
            }
        } else {
            if (LoginUtils.isAdmin()) {
                query = Trial.findTrialsByNameLike(criterias.getSearch(), sortFieldName, sortOrder);
                count = Trial.countFindTrialsByNameLike(criterias.getSearch());
            } else {
                query = Trial.findTrialsByNameLikeAndWebUser(criterias.getSearch(), webUser, sortFieldName, sortOrder);
                count = Trial.countFindTrialsByNameLikeAndWebUser(criterias.getSearch(), webUser);
            }
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }
}
