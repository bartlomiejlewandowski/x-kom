// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.web;

import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.model.WeightedPhrase;
import com.findwise.xkom.web.PhraseGroupController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect PhraseGroupController_Roo_Controller {
    
    @RequestMapping(params = "form", produces = "text/html")
    public String PhraseGroupController.createForm(Model uiModel) {
        populateEditForm(uiModel, new PhraseGroup());
        return "phrasegroups/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String PhraseGroupController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("phrasegroup", PhraseGroup.findPhraseGroup(id));
        uiModel.addAttribute("itemId", id);
        return "phrasegroups/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String PhraseGroupController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("phrasegroups", PhraseGroup.findPhraseGroupEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) PhraseGroup.countPhraseGroups() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("phrasegroups", PhraseGroup.findAllPhraseGroups(sortFieldName, sortOrder));
        }
        return "phrasegroups/list";
    }
    
    void PhraseGroupController.populateEditForm(Model uiModel, PhraseGroup phraseGroup) {
        uiModel.addAttribute("phraseGroup", phraseGroup);
        uiModel.addAttribute("grouptrials", GroupTrial.findAllGroupTrials());
        uiModel.addAttribute("webusers", WebUser.findAllWebUsers());
        uiModel.addAttribute("weightedphrases", WeightedPhrase.findAllWeightedPhrases());
    }
    
    String PhraseGroupController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
