package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.Trial;
import com.findwise.xkom.model.TrialRate;
import com.findwise.xkom.model.TrialRateBatchService;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.utils.ChartUtils;
import com.findwise.xkom.utils.DateUtils;
import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.utils.QueryUtils;
import com.findwise.xkom.utils.RequestUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

@RequestMapping("/trialrates")
@Controller
@RooWebScaffold(path = "trialrates", formBackingObject = TrialRate.class)
@GvNIXWebJpaBatch(service = TrialRateBatchService.class)
@GvNIXWebJQuery
@RooWebFinder
@GvNIXDatatables(ajax = true, inlineEditing = false, detailFields = { "rates" })
public class TrialRateController {

    private static final String SORT_BY = "runDate";
    private static final String SORT_ORDER = "DESC";

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid TrialRate trialRate, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        // user can't create trial rate
        return "redirect:/trialrates";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        // user can't create trial rate
        return "redirect:/trialrates";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid TrialRate trialRate, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        // user can't edit trial rate
        return "redirect:/trialrates";
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        // user can't edit trial rate
        return "redirect:/trialrates";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        TrialRate trialRate = TrialRate.findTrialRate(id);
        if (LoginUtils.isAdminOrLoggedIn(trialRate.getWebUser())) {
            trialRate.remove();
            uiModel.asMap().clear();
        }
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/trialrates";
    }

    public void setDatatablesBaseFilter(Map<String, Object> propertyMap) {
        if (!LoginUtils.isAdmin()) {
            propertyMap.put("webUser", LoginUtils.getCurrentUser());
        }
    }

    @RequestMapping(params = { "find=ByTrialAndRunDateBetweenAndWebUser", "form" }, method = RequestMethod.GET)
    public String findTrialRatesByTrialAndRunDateBetweenAndWebUserForm(Model uiModel) {
        List<String[]> dependencies = new ArrayList<String[]>();
        boolean isAdmin = LoginUtils.isAdmin();
        long trialsNum = isAdmin ? Trial.countTrials() : Trial.countFindTrialsByWebUser(LoginUtils.getCurrentUser());
        if (trialsNum == 0) {
            dependencies.add(new String[] { "trial", "trials" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        uiModel.addAttribute("trials", isAdmin ? Trial.findAllTrials()
                : Trial.findTrialsByWebUser(LoginUtils.getCurrentUser()).getResultList());
        addDateTimeFormatPatterns(uiModel);
        return "trialrates/findTrialRatesByTrialAndRunDateBetweenAndWebUser";
    }

    @RequestMapping(params = "find=ByTrialAndRunDateBetweenAndWebUser", method = RequestMethod.GET)
    public String findTrialRatesByTrialAndRunDateBetweenAndWebUser(@RequestParam("trial") List<Trial> trials,
            @RequestParam("minRunDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date minRunDate,
            @RequestParam("maxRunDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date maxRunDate,
            @RequestParam(value = "webUser", required = false) WebUser webUser,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sortFieldName", required = false) String sortFieldName,
            @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        boolean isAdmin = LoginUtils.isAdmin();
        webUser = LoginUtils.getCurrentUser();
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute(
                    "trialrates", isAdmin
                            ? TrialRate
                                    .findTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate,
                                            sortFieldName, sortOrder)
                                    .setFirstResult(firstResult).setMaxResults(sizeNo).getResultList()
                            : TrialRate
                                    .findTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate, maxRunDate,
                                            webUser, sortFieldName, sortOrder)
                                    .setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) (isAdmin
                    ? TrialRate.countFindTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate)
                    : TrialRate.countFindTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate, maxRunDate,
                            webUser))
                    / sizeNo;
            uiModel.addAttribute("maxPages",
                    (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("trialrates",
                    isAdmin ? TrialRate.findTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate,
                            sortFieldName, sortOrder).getResultList()
                            : TrialRate.findTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate,
                                    maxRunDate, webUser, sortFieldName, sortOrder).getResultList());
        }
        addDateTimeFormatPatterns(uiModel);
        return "trialrates/list";
    }

    @RequestMapping(params = { "find=ByWebUser", "form" }, method = RequestMethod.GET)
    public String findTrialRatesByWebUserForm(Model uiModel) {
        // disabled for now
        return "trialrates/list";
    }

    @RequestMapping(params = "find=ByWebUser", method = RequestMethod.GET)
    public String findTrialRatesByWebUser(@RequestParam("webUser") WebUser webUser,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sortFieldName", required = false) String sortFieldName,
            @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        // disabled for now
        return "trialrates/list";
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", params = "ajax_find=ByWebUser", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findTrialRatesByWebUser(
            @DatatablesParams DatatablesCriterias criterias, @RequestParam("webUser") WebUser webUser) {
        // disabled for now
        return null;
    }

    private List<Map<String, String>> retrieveData(DatatablesCriterias criterias, TrialRate trialRate,
            HttpServletRequest request) {
        if (Strings.isNullOrEmpty(request.getParameter("minRunDate"))) {
            // Cloned criteria in order to not paginate the results
            DatatablesCriterias noPaginationCriteria = new DatatablesCriterias(criterias.getSearch(), 0, null,
                    criterias.getColumnDefs(), criterias.getSortingColumnDefs(), criterias.getInternalCounter());
            // Do the search to obtain the data
            Map<String, Object> baseSearchValuesMap = getPropertyMap(trialRate, request);
            setDatatablesBaseFilter(baseSearchValuesMap);
            org.gvnix.web.datatables.query.SearchResults<com.findwise.xkom.model.TrialRate> searchResult = datatablesUtilsBean_dtt
                    .findByCriteria(TrialRate.class, noPaginationCriteria, baseSearchValuesMap);
            org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();
            addDateTimeFormatPatterns(uiModel);
            Map<String, Object> datePattern = uiModel.asMap();
            // Use ConversionService with the obtained data
            return datatablesUtilsBean_dtt
                    .populateDataSet(searchResult.getResults(), "id", searchResult.getTotalCount(),
                            searchResult.getResultsCount(), criterias.getColumnDefs(), datePattern)
                    .getRows();
        } else {
            Date minRunDate = DateUtils.stringToDate(request.getParameter("minRunDate"));
            Date maxRunDate = DateUtils.stringToDate(request.getParameter("maxRunDate"));
            TypedQuery<TrialRate> query = LoginUtils.isAdmin()
                    ? TrialRate.findTrialRatesByTrialsAndRunDateBetween(Lists.newArrayList(trialRate.getTrial()),
                            minRunDate, maxRunDate, null, null)
                    : TrialRate.findTrialRatesByTrialsAndRunDateBetweenAndWebUser(
                            Lists.newArrayList(trialRate.getTrial()), minRunDate, maxRunDate,
                            LoginUtils.getCurrentUser(), null, null);
            org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();
            addDateTimeFormatPatterns(uiModel);
            Map<String, Object> datePattern = uiModel.asMap();
            // Use ConversionService with the obtained data
            return datatablesUtilsBean_dtt.populateDataSet(query.getResultList(), "id", query.getMaxResults(),
                    query.getMaxResults(), criterias.getColumnDefs(), datePattern).getRows();
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public String listDatatables(Model uiModel, HttpServletRequest request) {
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        }
        String parentTableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(parentTableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", parentTableHashId);
        }
        String tableHashId = params.remove("dtt_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId) && !uiModel.containsAttribute("dtt_table_id_hash")) {
            uiModel.addAttribute("dtt_table_id_hash", tableHashId);
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", RequestUtils.mergeMultivalue(params, request));
        }

        // Add attribute available into view with information about each detail
        // datatables
        Map<String, String> details;
        List<Map<String, String>> detailsInfo = new ArrayList<Map<String, String>>(1);
        details = new HashMap<String, String>();
        // Base path for detail datatables entity (to get detail datatables
        // fragment URL)
        details.put("path", "rateitems");
        details.put("property", "rates");
        // Property name in detail entity with the relation to master entity
        details.put("mappedBy", "trialRate");
        detailsInfo.add(details);
        uiModel.addAttribute("detailsInfo", detailsInfo);

        // generate chart data
        if (params.containsKey("minRunDate")) {
            List<Trial> trials = Trial.findTrialsByIds(QueryUtils.convertIds(request.getParameterValues("trial")))
                    .getResultList();
            Date minRunDate = DateUtils.stringToDate(params.get("minRunDate"));
            Date maxRunDate = DateUtils.stringToDate(params.get("maxRunDate"));
            uiModel.addAttribute("chart",
                    ChartUtils
                            .generateChart(LoginUtils.isAdmin()
                                    ? TrialRate.findTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate,
                                            null, null).getResultList()
                                    : TrialRate
                                            .findTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate,
                                                    maxRunDate, LoginUtils.getCurrentUser(), null, null)
                                            .getResultList()));
        } else {
            uiModel.addAttribute("chart",
                    ChartUtils
                            .generateChart(LoginUtils.isAdmin() ? TrialRate.findAllTrialRates(SORT_BY, SORT_ORDER)
                                    : TrialRate
                                            .findTrialRatesByWebUser(LoginUtils.getCurrentUser(), SORT_BY, SORT_ORDER)
                                            .setMaxResults(5).getResultList()));
        }
        return "trialrates/list";
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllTrialRates(@DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute TrialRate trialRate, HttpServletRequest request) {
        WebUser webUser = LoginUtils.getCurrentUser();
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortField = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        TypedQuery<TrialRate> query = null;
        boolean isAdmin = LoginUtils.isAdmin();
        if (trialRate.getTrial() != null) {
            query = (isAdmin ? TrialRate.findTrialRatesByTrial(trialRate.getTrial(), sortField, sortOrder)
                    : TrialRate.findTrialRatesByTrialAndWebUser(trialRate.getTrial(), webUser, sortField, sortOrder))
                            .setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        } else {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM TrialRate AS o");
            QueryUtils.appendOrderClause(queryBuilder, TrialRate.fieldNames4OrderClauseFilter, sortField, sortOrder);
            query = (isAdmin ? TrialRate.entityManager().createQuery(queryBuilder.toString(), TrialRate.class)
                    : TrialRate.findTrialRatesByWebUser(webUser, sortField, sortOrder))
                            .setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        }
        // Get datatables required counts
        long totalRecords = trialRate.getTrial() != null
                ? (isAdmin ? TrialRate.countFindTrialRatesByTrial(trialRate.getTrial())
                        : TrialRate.countFindTrialRatesByTrialAndWebUser(trialRate.getTrial(), webUser))
                : (isAdmin ? TrialRate.countTrialRates() : TrialRate.countFindTrialRatesByWebUser(webUser));
        long recordsFound = totalRecords;
        // Entity pk field name
        String pkFieldName = "id";
        org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();
        addDateTimeFormatPatterns(uiModel);
        Map<String, Object> datePattern = uiModel.asMap();
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, totalRecords, recordsFound, criterias.getColumnDefs(), datePattern);
        return DatatablesResponse.build(dataSet, criterias);
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", params = "ajax_find=ByTrialAndRunDateBetweenAndWebUser", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findTrialRatesByTrialAndRunDateBetweenAndWebUser(
            @DatatablesParams DatatablesCriterias criterias, @RequestParam("trial") List<Trial> trials,
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam("minRunDate") Date minRunDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam("maxRunDate") Date maxRunDate,
            @RequestParam(value = "webUser", required = false) WebUser webUser) {
        webUser = LoginUtils.getCurrentUser();
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortField = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        boolean isAdmin = LoginUtils.isAdmin();
        TypedQuery<TrialRate> query = (isAdmin
                ? TrialRate.findTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate, sortField,
                        sortOrder)
                : TrialRate.findTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate, maxRunDate, webUser,
                        sortField, sortOrder)).setFirstResult(criterias.getDisplayStart())
                                .setMaxResults(criterias.getDisplaySize());
        // Get datatables required counts
        long totalRecords = isAdmin
                ? TrialRate.countFindTrialRatesByTrialsAndRunDateBetween(trials, minRunDate, maxRunDate)
                : TrialRate.countFindTrialRatesByTrialsAndRunDateBetweenAndWebUser(trials, minRunDate, maxRunDate,
                        webUser);
        long recordsFound = totalRecords;
        // Entity pk field name
        String pkFieldName = "id";
        org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();
        addDateTimeFormatPatterns(uiModel);
        Map<String, Object> datePattern = uiModel.asMap();
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, totalRecords, recordsFound, criterias.getColumnDefs(), datePattern);
        return DatatablesResponse.build(dataSet, criterias);
    }
}
