package com.findwise.xkom.cron;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.elastic.TrialExecutorService;
import com.findwise.xkom.model.GroupTrial;

/**
 * Group trial execution job
 * 
 * @author edward.miedzinski
 *
 */
@Component
public class PerformGroupTrialJob implements Job {

    private static final Logger log = Logger.getLogger(PerformGroupTrialJob.class);

    @Autowired
    private TrialExecutorService executor;

    @Override
    @Transactional
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Long trialId = context.getJobDetail().getJobDataMap().getLong("trialId");
        GroupTrial trial = GroupTrial.findGroupTrial(trialId);
        log.info(String.format("Executing group trial: '%s'", trial.getName()));
        executor.executeGroupTrial(trial, true);
    }
}
