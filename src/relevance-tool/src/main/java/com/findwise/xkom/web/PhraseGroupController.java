package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.gvnix.web.datatables.query.SearchResults;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.PhraseGroupBatchService;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.utils.LoginUtils;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;

@RequestMapping("/phrasegroups")
@Controller
@RooWebScaffold(path = "phrasegroups", formBackingObject = PhraseGroup.class)
@GvNIXWebJpaBatch(service = PhraseGroupBatchService.class)
@GvNIXWebJQuery
@GvNIXLoupeController
@GvNIXDatatables(inlineEditing = true, ajax = true, detailFields = { "phrases", "groupTrials" })
public class PhraseGroupController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid PhraseGroup phraseGroup, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, phraseGroup);
            return "phrasegroups/create";
        }
        if (PhraseGroup.countFindPhraseGroupsByName(phraseGroup.getName()) == 0) {
            phraseGroup.setWebUser(LoginUtils.getCurrentUser());
            uiModel.asMap().clear();
            phraseGroup.persist();
        }
        return "redirect:/phrasegroups";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid PhraseGroup phraseGroup, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, phraseGroup);
            return "phrasegroups/update";
        }
        phraseGroup.update();
        uiModel.asMap().clear();
        return "redirect:/phrasegroups/" + encodeUrlPathSegment(phraseGroup.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        PhraseGroup phraseGroup = PhraseGroup.findPhraseGroup(id);
        if (LoginUtils.isAdminOrLoggedIn(phraseGroup.getWebUser())) {
            populateEditForm(uiModel, PhraseGroup.findPhraseGroup(id));
        }
        return "phrasegroups/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        PhraseGroup phraseGroup = PhraseGroup.findPhraseGroup(id);
        if (LoginUtils.isAdminOrLoggedIn(phraseGroup.getWebUser())) {
            phraseGroup.remove();
        }
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/phrasegroups";
    }

    @RequestMapping(params = "my", method = RequestMethod.GET)
    public String findPhraseGroupsByLoggedUser(@RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sortFieldName", required = false) String sortFieldName,
            @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        WebUser webUser = LoginUtils.getCurrentUser();
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("my_groups", PhraseGroup.findPhraseGroupsByWebUser(webUser, sortFieldName, sortOrder)
                    .setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) PhraseGroup.countFindPhraseGroupsByWebUser(webUser) / sizeNo;
            uiModel.addAttribute("maxPages",
                    (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("my_groups",
                    PhraseGroup.findPhraseGroupsByWebUser(webUser, sortFieldName, sortOrder).getResultList());
        }
        uiModel.asMap().put("datatablesFiltered", true);
        // Add attribute available into view with information about each detail
        // datatables
        Map<String, String> details;
        List<Map<String, String>> detailsInfo = new ArrayList<Map<String, String>>(2);
        details = new HashMap<String, String>();
        // Base path for detail datatables entity (to get detail datatables
        // fragment URL)
        details.put("path", "weightedphrases");
        details.put("property", "phrases");
        // Property name in detail entity with the relation to master entity
        details.put("mappedBy", "phraseGroup");
        detailsInfo.add(details);
        details = new HashMap<String, String>();
        // Base path for detail datatables entity (to get detail datatables
        // fragment URL)
        details.put("path", "grouptrials");
        details.put("property", "groupTrials");
        // Property name in detail entity with the relation to master entity
        details.put("mappedBy", "phraseGroup");
        detailsInfo.add(details);
        uiModel.addAttribute("detailsInfo", detailsInfo);
        return "phrasegroups/list_my";
    }

    @RequestMapping(headers = "Accept=application/json", params = "filtered", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllPhraseGroupsByLoggedUser(
            @DatatablesParams DatatablesCriterias criterias, @ModelAttribute PhraseGroup phraseGroup,
            HttpServletRequest request) {
        // URL parameters are used as base search filters
        Map<String, Object> baseSearchValuesMap = getPropertyMap(phraseGroup, request);
        if (!LoginUtils.isAdmin()) {
            baseSearchValuesMap.put("webUser", LoginUtils.getCurrentUser());
        }
        setDatatablesBaseFilter(baseSearchValuesMap);
        SearchResults<PhraseGroup> searchResult = datatablesUtilsBean_dtt.findByCriteria(PhraseGroup.class, criterias,
                baseSearchValuesMap);
        // Get datatables required counts
        long totalRecords = searchResult.getTotalCount();
        long recordsFound = searchResult.getResultsCount();
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(searchResult.getResults(),
                pkFieldName, totalRecords, recordsFound, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }
}
