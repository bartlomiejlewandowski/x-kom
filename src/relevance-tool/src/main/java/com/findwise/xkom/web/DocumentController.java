package com.findwise.xkom.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.findwise.xkom.model.Document;
import com.findwise.xkom.model.DocumentBatchService;
import com.findwise.xkom.model.ProductImport;
import com.findwise.xkom.utils.ProductImportUtils;

@RequestMapping("/documents")
@Controller
@RooWebScaffold(path = "documents", formBackingObject = Document.class)
@GvNIXWebJpaBatch(service = DocumentBatchService.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, detailFields = { "rates" })
public class DocumentController {

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Document document, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, document);
            return "documents/update";
        }
        uiModel.asMap().clear();
        document.update();
        return "redirect:/documents/" + encodeUrlPathSegment(document.getId().toString(), httpServletRequest);
    }

    /**
     * Import
     */
    @RequestMapping(value = "/import", params = "form", produces = "text/html")
    public String importForm(Model uiModel) {
        populateImportForm(uiModel, new ProductImport());
        return "documents/import";
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST, consumes = "multipart/form-data", produces = "text/html")
    @Transactional
    public String importProducts(@Valid ProductImport productImport, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateImportForm(uiModel, productImport);
            return "documents/import";
        }
        try {
            List<Document> products = ProductImportUtils.getProducts(productImport.getFile());
            for (Document product : products) {
                if (Document.findDocumentsByElasticIdEquals(product.getElasticId()).getResultList().isEmpty()) {
                    product.persist();
                }
            }
        } catch (IOException e) {
            return "documents/import";
        }
        return "redirect:/documents";
    }

    void populateImportForm(Model uiModel, ProductImport productImport) {
        uiModel.addAttribute("productImport", productImport);
    }
}
