// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.model;

import com.findwise.xkom.model.GroupRateDetailedItem;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect GroupRateDetailedItem_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager GroupRateDetailedItem.entityManager;
    
    public static final List<String> GroupRateDetailedItem.fieldNames4OrderClauseFilter = java.util.Arrays.asList("ordinal", "phrase", "document", "rateValue", "rate", "groupRateItem");
    
    public static final EntityManager GroupRateDetailedItem.entityManager() {
        EntityManager em = new GroupRateDetailedItem().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long GroupRateDetailedItem.countGroupRateDetailedItems() {
        return entityManager().createQuery("SELECT COUNT(o) FROM GroupRateDetailedItem o", Long.class).getSingleResult();
    }
    
    public static List<GroupRateDetailedItem> GroupRateDetailedItem.findAllGroupRateDetailedItems() {
        return entityManager().createQuery("SELECT o FROM GroupRateDetailedItem o", GroupRateDetailedItem.class).getResultList();
    }
    
    public static List<GroupRateDetailedItem> GroupRateDetailedItem.findAllGroupRateDetailedItems(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM GroupRateDetailedItem o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, GroupRateDetailedItem.class).getResultList();
    }
    
    public static GroupRateDetailedItem GroupRateDetailedItem.findGroupRateDetailedItem(Long id) {
        if (id == null) return null;
        return entityManager().find(GroupRateDetailedItem.class, id);
    }
    
    public static List<GroupRateDetailedItem> GroupRateDetailedItem.findGroupRateDetailedItemEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM GroupRateDetailedItem o", GroupRateDetailedItem.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<GroupRateDetailedItem> GroupRateDetailedItem.findGroupRateDetailedItemEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM GroupRateDetailedItem o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, GroupRateDetailedItem.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void GroupRateDetailedItem.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void GroupRateDetailedItem.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            GroupRateDetailedItem attached = GroupRateDetailedItem.findGroupRateDetailedItem(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void GroupRateDetailedItem.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void GroupRateDetailedItem.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public GroupRateDetailedItem GroupRateDetailedItem.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        GroupRateDetailedItem merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
