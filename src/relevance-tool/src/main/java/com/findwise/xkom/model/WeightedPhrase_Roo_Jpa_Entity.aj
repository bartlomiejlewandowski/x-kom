// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.model;

import com.findwise.xkom.model.WeightedPhrase;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect WeightedPhrase_Roo_Jpa_Entity {
    
    declare @type: WeightedPhrase: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long WeightedPhrase.id;
    
    @Version
    @Column(name = "version")
    private Integer WeightedPhrase.version;
    
    public Long WeightedPhrase.getId() {
        return this.id;
    }
    
    public void WeightedPhrase.setId(Long id) {
        this.id = id;
    }
    
    public Integer WeightedPhrase.getVersion() {
        return this.version;
    }
    
    public void WeightedPhrase.setVersion(Integer version) {
        this.version = version;
    }
    
}
