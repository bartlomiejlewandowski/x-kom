// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.findwise.xkom.model;

import com.findwise.xkom.model.TrialRate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect TrialRate_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager TrialRate.entityManager;
    
    public static final List<String> TrialRate.fieldNames4OrderClauseFilter = java.util.Arrays.asList("id", "runDate", "trial", "rates", "webUser", "rateValue");
    
    public static final EntityManager TrialRate.entityManager() {
        EntityManager em = new TrialRate().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long TrialRate.countTrialRates() {
        return entityManager().createQuery("SELECT COUNT(o) FROM TrialRate o", Long.class).getSingleResult();
    }
    
    public static List<TrialRate> TrialRate.findAllTrialRates(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM TrialRate o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, TrialRate.class).getResultList();
    }
    
    public static TrialRate TrialRate.findTrialRate(Long id) {
        if (id == null) return null;
        return entityManager().find(TrialRate.class, id);
    }
    
    public static List<TrialRate> TrialRate.findTrialRateEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM TrialRate o", TrialRate.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<TrialRate> TrialRate.findTrialRateEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM TrialRate o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, TrialRate.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void TrialRate.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void TrialRate.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void TrialRate.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public TrialRate TrialRate.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        TrialRate merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
