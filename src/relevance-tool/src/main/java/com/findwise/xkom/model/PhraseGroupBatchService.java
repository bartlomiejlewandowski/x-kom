package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;

@Service
@GvNIXJpaBatch(entity = PhraseGroup.class)
public class PhraseGroupBatchService {

    @Transactional
    public int deleteAll() {
        List<PhraseGroup> groups = PhraseGroup.findAllPhraseGroups();
        delete(groups);
        return groups.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<PhraseGroup> groups = PhraseGroup.findPhraseGroupsByIds(ids).getResultList();
        delete(groups);
        return groups.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<PhraseGroup> phraseGroups) {
        for (PhraseGroup phrasegroup : phraseGroups) {
            if (PhraseGroup.countFindPhraseGroupsByName(phrasegroup.getName()) == 0) {
                phrasegroup.setWebUser(LoginUtils.getCurrentUser());
                phrasegroup.persist();
            }
        }
    }

    @Transactional
    public List<PhraseGroup> update(List<PhraseGroup> phrasegroups) {
        List<PhraseGroup> merged = new ArrayList<PhraseGroup>();
        for (PhraseGroup phrasegroup : phrasegroups) {
            if (LoginUtils.isAdminOrLoggedIn(phrasegroup.getWebUser())) {
                merged.add(phrasegroup.update());
            }
        }
        return merged;
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<PhraseGroup> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

    @Transactional
    public void delete(List<PhraseGroup> phrasegroups) {
        for (PhraseGroup phrasegroup : phrasegroups) {
            if (LoginUtils.isAdminOrLoggedIn(phrasegroup.getWebUser())) {
                phrasegroup.remove();
            }
        }
    }

}
