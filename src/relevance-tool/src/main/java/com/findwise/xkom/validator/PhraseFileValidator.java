package com.findwise.xkom.validator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.findwise.xkom.utils.PhraseImportUtils;

public class PhraseFileValidator implements ConstraintValidator<PhraseFile, Object> {

    private static final Logger log = Logger.getLogger(PhraseFileValidator.class);

    private Set<String> contentTypes;

    @Override
    public void initialize(PhraseFile constraintAnnotation) {
        contentTypes = new HashSet<>(Arrays.asList(constraintAnnotation.contentTypes()));
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        MultipartFile file = (MultipartFile) value;
        if (!contentTypes.contains(file.getContentType())) {
            log.error(String.format("Wrong content type of imported file. Expected one of '%s', is: '%s'",
                    Arrays.toString(contentTypes.toArray()), file.getContentType()));
            return false;
        }
        return PhraseImportUtils.hasValidContent(file);
    }
}
