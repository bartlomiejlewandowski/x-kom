package com.findwise.xkom.model;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;

@Service
@GvNIXJpaBatch(entity = WebUser.class)
public class WebUserBatchService {
}
