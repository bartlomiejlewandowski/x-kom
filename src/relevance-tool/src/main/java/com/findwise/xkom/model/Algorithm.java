package com.findwise.xkom.model;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.findwise.xkom.validator.IsJson;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findAlgorithmsByParameters" })
public class Algorithm {

    /**
     */
    @NotNull
    @NotEmpty
    private String name;

    /**
     */
    @NotNull
    @IsJson
    @Column(length = 20000)
    private String parameters;
}
