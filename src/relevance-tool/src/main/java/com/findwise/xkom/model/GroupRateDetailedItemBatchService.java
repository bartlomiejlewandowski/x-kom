package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.services.RateService;
import com.google.common.collect.Lists;

@Service
@GvNIXJpaBatch(entity = GroupRateDetailedItem.class)
public class GroupRateDetailedItemBatchService {

    @Autowired
    private RateService rateService;

    @Transactional
    public List<GroupRateDetailedItem> update(List<GroupRateDetailedItem> groupratedetaileditems) {
        List<GroupRateDetailedItem> merged = new ArrayList<GroupRateDetailedItem>();
        List<Rate> ratesToUpdate = Lists.newArrayList();
        for (GroupRateDetailedItem item : groupratedetaileditems) {
            GroupRateDetailedItem oldItem = GroupRateDetailedItem.findGroupRateDetailedItem(item.getId());
            item.setOrdinal(oldItem.getOrdinal());
            item.setPhrase(oldItem.getPhrase());
            item.setDocument(oldItem.getDocument());
            item.setRate(oldItem.getRate());
            item.setGroupRateItem(oldItem.getGroupRateItem());
            ratesToUpdate.add(item.getRate());
            item.getRate().setRateValue(item.getRateValue());
            merged.add(item.merge());
        }
        rateService.updateRates(ratesToUpdate);
        return merged;
    }
}
