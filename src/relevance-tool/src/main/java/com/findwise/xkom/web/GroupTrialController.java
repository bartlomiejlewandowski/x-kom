package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.cron.SchedulerService;
import com.findwise.xkom.elastic.TrialExecutorService;
import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.GroupTrialBatchService;
import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.WebUser;
import com.findwise.xkom.utils.LoginUtils;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;

@RequestMapping("/grouptrials")
@Controller
@RooWebScaffold(path = "grouptrials", formBackingObject = GroupTrial.class)
@GvNIXWebJpaBatch(service = GroupTrialBatchService.class)
@GvNIXWebJQuery
@GvNIXLoupeController
@GvNIXDatatables(ajax = true, detailFields = { "rates" })
public class GroupTrialController {

    @Autowired
    private TrialExecutorService executor;

    @Autowired
    private SchedulerService scheduler;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid GroupTrial groupTrial, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, groupTrial);
            return "grouptrials/create";
        }
        groupTrial.setWebUser(LoginUtils.getCurrentUser());
        uiModel.asMap().clear();
        groupTrial.persist();
        scheduler.scheduleGroupTrial(groupTrial);

        return "redirect:/grouptrials/" + encodeUrlPathSegment(groupTrial.getId().toString(), httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid GroupTrial groupTrial, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, groupTrial);
            return "grouptrials/update";
        }
        GroupTrial oldTrial = GroupTrial.findGroupTrial(groupTrial.getId());
        if (LoginUtils.isAdminOrLoggedIn(oldTrial.getWebUser())) {
            groupTrial.setWebUser(oldTrial.getWebUser());
            groupTrial.setRates(oldTrial.getRates());
            uiModel.asMap().clear();
            groupTrial.merge();
            scheduler.scheduleGroupTrial(groupTrial);
        }
        return "redirect:/grouptrials/" + encodeUrlPathSegment(groupTrial.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        GroupTrial trial = GroupTrial.findGroupTrial(id);
        // check if user can see the trial
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            uiModel.addAttribute("grouptrial", trial);
            uiModel.addAttribute("itemId", id);
        }
        return "grouptrials/show";
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        GroupTrial trial = GroupTrial.findGroupTrial(id);
        // check if user can update the trial
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            populateEditForm(uiModel, trial);
        }
        return "grouptrials/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        GroupTrial groupTrial = GroupTrial.findGroupTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(groupTrial.getWebUser())) {
            groupTrial.remove();
            scheduler.unscheduleJob(groupTrial.getId().toString(), SchedulerService.GROUP_TRIAL_GROUP);
        }
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/grouptrials";
    }

    @RequestMapping(value = "/{id}", params = "run", produces = "text/html")
    public String runAction(@PathVariable("id") Long id, Model uiModel, HttpServletRequest httpServletRequest) {
        GroupTrial trial = GroupTrial.findGroupTrial(id);
        if (LoginUtils.isAdminOrLoggedIn(trial.getWebUser())) {
            executor.executeGroupTrial(trial, true);
        }
        return "redirect:/grouptrials";
    }

    public void setDatatablesBaseFilter(Map<String, Object> propertyMap) {
        if (!LoginUtils.isAdmin()) {
            propertyMap.put("webUser", LoginUtils.getCurrentUser());
        }
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "GroupTrial_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");
        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllGroupTrials(@DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute GroupTrial trial, HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        WebUser webUser = LoginUtils.getCurrentUser();
        PhraseGroup group = trial.getPhraseGroup();

        TypedQuery<GroupTrial> query = null;
        Long count = 0l;
        if (group == null && criterias.getSearch().isEmpty()) {
            if (LoginUtils.isAdmin()) {
                StringBuilder queryBuilder = new StringBuilder("SELECT o FROM GroupTrial AS o");
                QueryUtils.appendOrderClause(queryBuilder, GroupTrial.fieldNames4OrderClauseFilter, sortFieldName,
                        sortOrder);
                query = GroupTrial.entityManager().createQuery(queryBuilder.toString(), GroupTrial.class);
                count = GroupTrial.countGroupTrials();
            } else {
                query = GroupTrial.findGroupTrialsByWebUser(webUser, sortFieldName, sortOrder);
                count = GroupTrial.countFindGroupTrialsByWebUser(webUser);
            }
        } else if (group != null && !criterias.getSearch().isEmpty()) {
            if (LoginUtils.isAdmin()) {
                query = GroupTrial.findGroupTrialsByNameLikeAndPhraseGroup(criterias.getSearch(), group, sortFieldName,
                        sortOrder);
                count = GroupTrial.countFindGroupTrialsByNameLikeAndPhraseGroup(criterias.getSearch(), group);
            } else {
                query = GroupTrial.findGroupTrialsByNameLikeAndPhraseGroupAndWebUser(criterias.getSearch(), group,
                        webUser, sortFieldName, sortOrder);
                count = GroupTrial.countFindGroupTrialsByNameLikeAndPhraseGroupAndWebUser(criterias.getSearch(), group,
                        webUser);
            }
        } else if (group != null) {
            if (LoginUtils.isAdmin()) {
                query = GroupTrial.findGroupTrialsByPhraseGroup(group, sortFieldName, sortOrder);
                count = GroupTrial.countFindGroupTrialsByPhraseGroup(group);
            } else {
                query = GroupTrial.findGroupTrialsByPhraseGroupAndWebUser(group, webUser, sortFieldName, sortOrder);
                count = GroupTrial.countFindGroupTrialsByPhraseGroupAndWebUser(group, webUser);
            }
        } else {
            if (LoginUtils.isAdmin()) {
                query = GroupTrial.findGroupTrialsByNameLike(criterias.getSearch(), sortFieldName, sortOrder);
                count = GroupTrial.countFindGroupTrialsByNameLike(criterias.getSearch());
            } else {
                query = GroupTrial.findGroupTrialsByNameLikeAndWebUser(criterias.getSearch(), webUser, sortFieldName,
                        sortOrder);
                count = GroupTrial.countFindGroupTrialsByNameLikeAndWebUser(criterias.getSearch(), webUser);
            }
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }
}
