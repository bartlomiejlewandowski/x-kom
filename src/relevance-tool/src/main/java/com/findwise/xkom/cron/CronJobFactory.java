package com.findwise.xkom.cron;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CronJobFactory implements JobFactory {

    @Autowired
    private PerformTrialJob trialJob;

    @Autowired
    private PerformGroupTrialJob groupTrialJob;

    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        JobDetail jobDetail = triggerFiredBundle.getJobDetail();
        Class<? extends Job> jobClass = jobDetail.getJobClass();
        if (jobClass.equals(PerformTrialJob.class)) {
            return trialJob;
        } else if (jobClass.equals(PerformGroupTrialJob.class)) {
            return groupTrialJob;
        }
        throw new SchedulerException("Unsupported job class");
    }
}