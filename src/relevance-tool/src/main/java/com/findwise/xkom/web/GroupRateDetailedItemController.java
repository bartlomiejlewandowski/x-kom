package com.findwise.xkom.web;

import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.GroupRateDetailedItem;
import com.findwise.xkom.model.GroupRateDetailedItemBatchService;
import com.findwise.xkom.model.GroupRateItem;
import com.findwise.xkom.services.RateService;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

@RequestMapping("/groupratedetaileditems")
@Controller
@RooWebScaffold(path = "groupratedetaileditems", formBackingObject = GroupRateDetailedItem.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, inlineEditing = true)
@GvNIXWebJpaBatch(service = GroupRateDetailedItemBatchService.class)
public class GroupRateDetailedItemController {

    @Autowired
    private RateService rateService;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid GroupRateDetailedItem groupRateDetailedItem, BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        throw new UnsupportedOperationException();
    }

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllGroupRateDetailedItems(
            @DatatablesParams DatatablesCriterias criterias,
            @ModelAttribute GroupRateDetailedItem groupRateDetailedItem, HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        GroupRateItem groupRateItem = groupRateDetailedItem.getGroupRateItem();
        String search = Strings.isNullOrEmpty(criterias.getSearch()) ? null : criterias.getSearch();
        TypedQuery<GroupRateDetailedItem> query = null;
        Long count = 0l;
        String[] searchFields = new String[] { "document.name" };
        if (groupRateItem == null && search == null) {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM GroupRateDetailedItem AS o");
            QueryUtils.appendOrderClause(queryBuilder, GroupRateDetailedItem.fieldNames4OrderClauseFilter,
                    sortFieldName, sortOrder);
            query = GroupRateDetailedItem.entityManager().createQuery(queryBuilder.toString(),
                    GroupRateDetailedItem.class);
            count = GroupRateDetailedItem.countGroupRateDetailedItems();
        } else if (groupRateItem != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("groupRateItem", groupRateItem);
            Pair<TypedQuery<GroupRateDetailedItem>, Long> resultInfo = QueryUtils.getResultInfo(
                    GroupRateDetailedItem.entityManager(), "GroupRateDetailedItem", filters, searchFields, search,
                    GroupRateDetailedItem.fieldNames4OrderClauseFilter, sortFieldName, sortOrder,
                    GroupRateDetailedItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (groupRateItem != null && search == null) {
            query = GroupRateDetailedItem.findGroupRateDetailedItemsByGroupRateItem(groupRateItem, sortFieldName,
                    sortOrder);
            count = GroupRateDetailedItem.countFindGroupRateDetailedItemsByGroupRateItem(groupRateItem);
        } else {
            Map<String, Object> filters = Maps.newHashMap();
            Pair<TypedQuery<GroupRateDetailedItem>, Long> resultInfo = QueryUtils.getResultInfo(
                    GroupRateDetailedItem.entityManager(), "GroupRateDetailedItem", filters, searchFields, search,
                    GroupRateDetailedItem.fieldNames4OrderClauseFilter, sortFieldName, sortOrder,
                    GroupRateDetailedItem.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }

    public void populateItemForRender(HttpServletRequest request, GroupRateDetailedItem groupRateDetailedItem,
            boolean editing) {
        org.springframework.ui.Model uiModel = new org.springframework.ui.ExtendedModelMap();

        request.setAttribute("groupRateDetailedItem", groupRateDetailedItem);
        request.setAttribute("itemId", conversionService_dtt.convert(groupRateDetailedItem.getId(), String.class));

        if (editing) {
            // spring from:input tag uses BindingResult to locate property
            // editors for each bean
            // property. So, we add a request attribute (required key id
            // BindingResult.MODEL_KEY_PREFIX + object name)
            // with a correctly initialized bindingResult.
            BeanPropertyBindingResult bindindResult = new BeanPropertyBindingResult(groupRateDetailedItem,
                    "groupRateDetailedItem");
            bindindResult.initConversion(conversionService_dtt);
            request.setAttribute(BindingResult.MODEL_KEY_PREFIX + "groupRateDetailedItem", bindindResult);
            // Add date time patterns and enums to populate inputs
            populateEditForm(uiModel, groupRateDetailedItem);
        }

        // Load uiModel attributes into request
        Map<String, Object> modelMap = uiModel.asMap();
        for (String key : modelMap.keySet()) {
            request.setAttribute(key, modelMap.get(key));
        }
        request.setAttribute("rateValues", rateService.populateRateValues());
    }

    @ModelAttribute
    public void populateDatatablesConfig(Model uiModel) {
        uiModel.addAttribute("datatablesHasBatchSupport", true);
        uiModel.addAttribute("datatablesUseAjax", true);
        uiModel.addAttribute("datatablesInlineEditing", true);
        uiModel.addAttribute("datatablesInlineCreating", false);
        uiModel.addAttribute("datatablesSecurityApplied", true);
        uiModel.addAttribute("datatablesStandardMode", true);
        uiModel.addAttribute("finderNameParam", "ajax_find");
    }

    void populateEditForm(Model uiModel, GroupRateDetailedItem groupRateDetailedItem) {
        uiModel.addAttribute("groupRateDetailedItem", groupRateDetailedItem);
    }
}
