package com.findwise.xkom.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.findwise.xkom.model.Document;

public class ProductImportUtils extends ImportUtils {

    private static final int COLUMNS_COUNT = 2;

    /**
     * Create products list (list of documents)
     * 
     * @param file
     *            import file
     * @return products list
     * @throws IOException
     */
    public static List<Document> getProducts(MultipartFile file) throws IOException {
        List<Document> result = new ArrayList<>();
        List<CSVRecord> records = getRecords(file);
        for (CSVRecord record : records) {
            if (record.size() >= COLUMNS_COUNT) {
                Document doc = new Document();
                doc.setElasticId(record.get(0));
                doc.setName(record.get(1));
                doc.setMetadata(records.size() >= 3 ? record.get(2) : null);
                result.add(doc);
            }
        }
        return result;
    }

    public static boolean hasValidContent(MultipartFile file) {
        return hasValidContent(COLUMNS_COUNT, file, true);
    }
}
