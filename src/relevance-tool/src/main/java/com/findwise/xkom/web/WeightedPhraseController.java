package com.findwise.xkom.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;
import org.gvnix.addon.loupefield.annotations.GvNIXLoupeController;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.findwise.xkom.model.PhraseGroup;
import com.findwise.xkom.model.WeightedPhrase;
import com.findwise.xkom.model.WeightedPhraseBatchService;
import com.findwise.xkom.utils.QueryUtils;
import com.github.dandelion.datatables.core.ajax.ColumnDef;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.github.dandelion.datatables.core.ajax.DatatablesResponse;
import com.github.dandelion.datatables.extras.spring3.ajax.DatatablesParams;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

@RequestMapping("/weightedphrases")
@Controller
@RooWebScaffold(path = "weightedphrases", formBackingObject = WeightedPhrase.class)
@GvNIXWebJQuery
@GvNIXDatatables(ajax = true, inlineEditing = true)
@GvNIXLoupeController
@GvNIXWebJpaBatch(service = WeightedPhraseBatchService.class)
public class WeightedPhraseController {

    @RequestMapping(headers = "Accept=application/json", value = "/datatables/ajax", produces = "application/json")
    @ResponseBody
    public DatatablesResponse<Map<String, String>> findAllWeightedPhrases(
            @DatatablesParams DatatablesCriterias criterias, @ModelAttribute WeightedPhrase phrase,
            HttpServletRequest request) {
        ColumnDef firstSortCol = criterias.getSortingColumnDefs().get(0);
        String sortFieldName = firstSortCol.getName();
        String sortOrder = firstSortCol.getSortDirection().name();
        PhraseGroup phraseGroup = phrase.getPhraseGroup();
        String search = Strings.isNullOrEmpty(criterias.getSearch()) ? null : criterias.getSearch();
        TypedQuery<WeightedPhrase> query = null;
        Long count = 0l;
        String[] searchFields = new String[] { "phrase.text" };
        if (phraseGroup == null && criterias.getSearch().isEmpty()) {
            StringBuilder queryBuilder = new StringBuilder("SELECT o FROM WeightedPhrase AS o");
            QueryUtils.appendOrderClause(queryBuilder, WeightedPhrase.fieldNames4OrderClauseFilter, sortFieldName,
                    sortOrder);
            query = WeightedPhrase.entityManager().createQuery(queryBuilder.toString(), WeightedPhrase.class);
            count = WeightedPhrase.countWeightedPhrases();
        } else if (phraseGroup != null && search != null) {
            Map<String, Object> filters = Maps.newHashMap();
            filters.put("phraseGroup", phraseGroup);
            Pair<TypedQuery<WeightedPhrase>, Long> resultInfo = QueryUtils.getResultInfo(WeightedPhrase.entityManager(),
                    "WeightedPhrase", filters, searchFields, search, WeightedPhrase.fieldNames4OrderClauseFilter,
                    sortFieldName, sortOrder, WeightedPhrase.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        } else if (phraseGroup != null) {
            query = WeightedPhrase.findWeightedPhrasesByPhraseGroup(phraseGroup, sortFieldName, sortOrder);
            count = WeightedPhrase.countFindWeightedPhrasesByPhraseGroup(phraseGroup);
        } else {
            Map<String, Object> filters = Maps.newHashMap();
            Pair<TypedQuery<WeightedPhrase>, Long> resultInfo = QueryUtils.getResultInfo(WeightedPhrase.entityManager(),
                    "WeightedPhrase", filters, searchFields, search, WeightedPhrase.fieldNames4OrderClauseFilter,
                    sortFieldName, sortOrder, WeightedPhrase.class);
            query = resultInfo.getKey();
            count = resultInfo.getRight();
        }
        query.setFirstResult(criterias.getDisplayStart()).setMaxResults(criterias.getDisplaySize());
        // Entity pk field name
        String pkFieldName = "id";
        DataSet<Map<String, String>> dataSet = datatablesUtilsBean_dtt.populateDataSet(query.getResultList(),
                pkFieldName, count, count, criterias.getColumnDefs(), null);
        return DatatablesResponse.build(dataSet, criterias);
    }

    @RequestMapping(params = "selector", produces = "text/html")
    public String showOnlyList(Model uiModel, HttpServletRequest request, @RequestParam("path") String listPath) {
        // as we can't get target table configuration lets use standard _ajax_
        // configuration.
        uiModel.asMap().put("datatablesHasBatchSupport", false);
        uiModel.asMap().put("datatablesUseAjax", true);
        uiModel.asMap().put("datatablesInlineEditing", false);
        uiModel.asMap().put("datatablesInlineCreating", false);
        uiModel.asMap().put("datatablesSecurityApplied", false);
        uiModel.asMap().put("datatablesStandardMode", true);
        uiModel.asMap().put("finderNameParam", "ajax_find");
        // Do common datatables operations: get entity list filtered by request
        // parameters
        Map<String, String> params = populateParametersMap(request);
        // Get parentId information for details render
        String parentId = params.remove("_dt_parentId");
        if (StringUtils.isNotBlank(parentId)) {
            uiModel.addAttribute("parentId", parentId);
        } else {
            uiModel.addAttribute("parentId", "WeightedPhrase_selector");
        }
        String rowOnTopIds = params.remove("dtt_row_on_top_ids");
        if (StringUtils.isNotBlank(rowOnTopIds)) {
            uiModel.addAttribute("dtt_row_on_top_ids", rowOnTopIds);
        }
        String tableHashId = params.remove("dtt_parent_table_id_hash");
        if (StringUtils.isNotBlank(tableHashId)) {
            uiModel.addAttribute("dtt_parent_table_id_hash", tableHashId);
        }
        params.remove("selector");
        params.remove("path");
        if (!params.isEmpty()) {
            uiModel.addAttribute("baseFilter", params);
        }
        uiModel.addAttribute("dtt_ignoreParams", new ArrayList<>(Arrays.asList("selector", "path")));
        uiModel.addAttribute("dtt_disableEditing", "true");
        // Show only the list fragment (without footer, header, menu, etc.)
        return "forward:/WEB-INF/views/" + listPath + ".jspx";
    }
}
