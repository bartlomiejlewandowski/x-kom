package com.findwise.xkom.elastic;

import com.findwise.xkom.model.GroupTrial;
import com.findwise.xkom.model.GroupTrialRate;
import com.findwise.xkom.model.Trial;
import com.findwise.xkom.model.TrialRate;

/**
 * Performs search and creates rate objects in DB
 * 
 * @author edward.miedzinski
 *
 */
public interface TrialExecutorService {

    /**
     * Execute trial and persist results when {@code persistResults} enabled
     * 
     * @param trial
     *            trial to execute
     * @param persistResults
     *            whether to persist results
     * @return trial rate object
     */
    public TrialRate executeTrial(Trial trial, boolean persistResults);

    /**
     * Execute group trial and persist results when {@code persistResults}
     * enabled
     * 
     * @param groupTrial
     *            trial to execute
     * @param persistResults
     *            whether to persist results
     * @return group trial rate object
     */
    public GroupTrialRate executeGroupTrial(GroupTrial groupTrial, boolean persistResults);

}
