package com.findwise.xkom.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.findwise.xkom.model.Chart;
import com.findwise.xkom.model.Chart.Series;
import com.findwise.xkom.model.GroupTrialRate;
import com.findwise.xkom.model.TrialRate;

public class ChartUtils {

    public static Chart<Long, BigDecimal> generateChart(List<TrialRate> rates) {
        Chart<Long, BigDecimal> chart = new Chart<>();
        chart.setTitle("");
        chart.setZoomEnabled(true);
        List<Series<Long, BigDecimal>> series = new ArrayList<>();
        Map<String, List<TrialRate>> rateMap = new HashMap<>();
        for (TrialRate rate : rates) {
            String trialName = rate.getTrial().getName();
            if (!rateMap.containsKey(trialName)) {
                rateMap.put(trialName, new ArrayList<TrialRate>());
            }
            rateMap.get(trialName).add(rate);
        }
        for (Map.Entry<String, List<TrialRate>> rateEntry : rateMap.entrySet()) {
            Series<Long, BigDecimal> newSeries = new Series<>();
            newSeries.setName(rateEntry.getKey());
            newSeries.setShowInLegend(true);
            newSeries.setType("line");
            newSeries.setxValueType("dateTime");
            Collections.sort(rateEntry.getValue());
            for (TrialRate rate : rateEntry.getValue()) {
                newSeries.addPoint(
                        new ImmutablePair<Long, BigDecimal>(rate.getRunDate().getTime(), rate.getRateValue()));
            }
            series.add(newSeries);
        }
        chart.setSeries(series);
        return chart;
    }

    public static Chart<Long, BigDecimal> generateGroupChart(List<GroupTrialRate> rates) {
        Chart<Long, BigDecimal> chart = new Chart<>();
        chart.setTitle("");
        chart.setZoomEnabled(true);
        List<Series<Long, BigDecimal>> series = new ArrayList<>();
        Map<String, List<GroupTrialRate>> rateMap = new HashMap<>();
        for (GroupTrialRate rate : rates) {
            String trialName = rate.getTrial().getName();
            if (!rateMap.containsKey(trialName)) {
                rateMap.put(trialName, new ArrayList<GroupTrialRate>());
            }
            rateMap.get(trialName).add(rate);
        }
        for (Map.Entry<String, List<GroupTrialRate>> rateEntry : rateMap.entrySet()) {
            Series<Long, BigDecimal> newSeries = new Series<>();
            newSeries.setName(rateEntry.getKey());
            newSeries.setShowInLegend(true);
            newSeries.setType("line");
            newSeries.setxValueType("dateTime");
            Collections.sort(rateEntry.getValue());
            for (GroupTrialRate rate : rateEntry.getValue()) {
                newSeries.addPoint(
                        new ImmutablePair<Long, BigDecimal>(rate.getRunDate().getTime(), rate.getRateValue()));
            }
            series.add(newSeries);
        }
        chart.setSeries(series);
        return chart;
    }
}
