package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.types.path.PathBuilder;

@Service
@GvNIXJpaBatch(entity = Server.class)
public class ServerBatchService {

    @Transactional
    public int deleteAll() {
        if (!LoginUtils.isAdmin()) {
            return 0;
        }
        return entityManager().createQuery("DELETE FROM Server").executeUpdate();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        if (!LoginUtils.isAdmin()) {
            return 0;
        }
        Query query = entityManager().createQuery("DELETE FROM Server as s WHERE s.id IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        if (!LoginUtils.isAdmin()) {
            return 0;
        }
        Query query = entityManager().createQuery("DELETE FROM Server as s WHERE s.id NOT IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }

    @Transactional
    public void create(List<Server> servers) {
        if (!LoginUtils.isAdmin()) {
            return;
        }
        for (Server server : servers) {
            server.persist();
        }
    }

    @Transactional
    public List<Server> update(List<Server> servers) {
        if (!LoginUtils.isAdmin()) {
            return Lists.newArrayList();
        }
        List<Server> merged = new ArrayList<Server>();
        for (Server server : servers) {
            merged.add(server.merge());
        }
        return merged;
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        if (!LoginUtils.isAdmin()) {
            return 0;
        }

        // if there no is a filter
        if (propertyValues == null || propertyValues.isEmpty()) {
            throw new IllegalArgumentException("Missing property values");
        }
        // Prepare a predicate
        BooleanBuilder baseFilterPredicate = new BooleanBuilder();

        // Base filter. Using BooleanBuilder, a cascading builder for
        // Predicate expressions
        PathBuilder<Server> entity = new PathBuilder<Server>(Server.class, "entity");

        // Build base filter
        for (String key : propertyValues.keySet()) {
            baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
        }

        // Create a query with filter
        JPADeleteClause delete = new JPADeleteClause(Server.entityManager(), entity);

        // execute delete
        return delete.where(baseFilterPredicate).execute();
    }

    @Transactional
    public void delete(List<Server> servers) {
        if (!LoginUtils.isAdmin()) {
            return;
        }
        for (Server server : servers) {
            server.remove();
        }
    }

}
