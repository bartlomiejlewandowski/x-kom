package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.findwise.xkom.utils.LoginUtils;

@Service
@GvNIXJpaBatch(entity = GroupTrialRate.class)
public class GroupTrialRateBatchService {

    @Transactional
    public int deleteAll() {
        List<GroupTrialRate> rates = GroupTrialRate.findAllGroupTrialRates();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<GroupTrialRate> rates = GroupTrialRate.findGroupTrialRatesByIds(ids).getResultList();
        delete(rates);
        return rates.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public void create(List<GroupTrialRate> groupTrialRates) {
        // user can't create group trial rate
        return;
    }

    @Transactional
    public List<GroupTrialRate> update(List<GroupTrialRate> grouptrialrates) {
        // user can't edit group trial rate
        return new ArrayList<GroupTrialRate>();
    }

    public List<GroupTrialRate> findByValues(Map<String, Object> propertyValues) {
        WebUser webUser = LoginUtils.getCurrentUser();
        if (propertyValues != null && !propertyValues.isEmpty()) {
            if (propertyValues.containsKey("trial")) {
                return GroupTrialRate
                        .findGroupTrialRatesByTrialAndWebUser((GroupTrial) propertyValues.get("trial"), webUser)
                        .getResultList();
            }
        }

        // no filter: return all elements
        return GroupTrialRate.findGroupTrialRatesByWebUser(webUser).getResultList();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<GroupTrialRate> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

    @Transactional
    public void delete(List<GroupTrialRate> grouptrialrates) {
        for (GroupTrialRate grouptrialrate : grouptrialrates) {
            if (LoginUtils.isAdminOrLoggedIn(grouptrialrate.getWebUser())) {
                grouptrialrate.remove();
            }
        }
    }
}
