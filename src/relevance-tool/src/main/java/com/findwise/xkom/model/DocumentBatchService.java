package com.findwise.xkom.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@GvNIXJpaBatch(entity = Document.class)
public class DocumentBatchService {

    @Transactional
    public List<Document> update(List<Document> documents) {
        List<Document> merged = new ArrayList<Document>();
        for (Document document : documents) {
            merged.add(document.update());
        }
        return merged;
    }

    @Transactional
    public int deleteAll() {
        List<Document> documents = Document.findAllDocuments();
        delete(documents);
        return documents.size();
    }

    @Transactional
    public int deleteIn(List<Long> ids) {
        List<Document> documents = Document.findDocumentsByIds(ids).getResultList();
        delete(documents);
        return documents.size();
    }

    @Transactional
    public int deleteNotIn(List<Long> ids) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    public long deleteByValues(Map<String, Object> propertyValues) {
        List<Document> items = findByValues(propertyValues);
        int count = items.size();
        delete(items);
        return count;
    }

}
