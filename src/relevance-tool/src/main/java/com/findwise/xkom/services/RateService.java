package com.findwise.xkom.services;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.collections.KeyValue;
import org.apache.commons.collections.keyvalue.DefaultKeyValue;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findwise.xkom.config.Configuration;
import com.findwise.xkom.model.GroupRateDetailedItem;
import com.findwise.xkom.model.GroupRateItem;
import com.findwise.xkom.model.Rate;
import com.findwise.xkom.model.RateItem;

@Service
public class RateService {

    private static final Logger log = Logger.getLogger(RateService.class);

    @Autowired
    private Configuration config;

    @Autowired
    private DataSource dataSource;

    /**
     * Calculate rate
     * 
     * @param rates
     * @return
     */
    public BigDecimal calculateRate(Set<RateItem> rates) {
        if (rates.isEmpty()) {
            return BigDecimal.ZERO;
        }
        double rateValue = 0;
        for (RateItem rateItem : rates) {
            rateValue += rateItem.getRate().getRateValue() * getWeightForResult(rateItem.getOrdinal());
        }
        return BigDecimal.valueOf(rateValue);
    }

    /**
     * Calculate rate for a phrase in a group
     * 
     * @param rates
     * @return
     */
    public BigDecimal calculateDetailedGroupRate(Set<GroupRateDetailedItem> rates) {
        if (rates.isEmpty()) {
            return BigDecimal.ZERO;
        }
        double rateValue = 0;
        for (GroupRateDetailedItem rateItem : rates) {
            rateValue += rateItem.getRate().getRateValue() * rateItem.getPhrase().getWeight().doubleValue()
                    * getWeightForResult(rateItem.getOrdinal());
        }
        return BigDecimal.valueOf(rateValue);
    }

    /**
     * Calculate rate for group
     * 
     * @param rates
     * @return
     */
    public BigDecimal calculateGroupRate(Set<GroupRateItem> rates) {
        if (rates.isEmpty()) {
            return BigDecimal.ZERO;
        }
        double rateValue = 0;
        for (GroupRateItem rateItem : rates) {
            rateValue += rateItem.getRateValue().doubleValue();
        }
        return BigDecimal.valueOf(rateValue / rates.size());
    }

    /**
     * Persist new rate if it does not exist in DB, update value otherwise
     * 
     * @param rate
     *            new rate to store
     */
    public void storeRate(Rate rate) {
        List<Rate> results = Rate.findRatesByDocumentAndPhrase(rate.getDocument(), rate.getPhrase()).getResultList();
        if (!results.isEmpty()) {
            // if rate exists, do not add the second one, just update the
            // rate value
            Rate oldRate = results.get(0);
            oldRate.setRateValue(rate.getRateValue());
            oldRate.merge();
        } else {
            rate.persist();
        }
    }

    /**
     * Update trial rates
     * 
     * @param rates
     */
    public void updateRates(List<Rate> rates) {
        Long[] ids = new Long[rates.size()];
        for (int i = 0; i < rates.size(); ++i) {
            ids[i] = rates.get(i).getId();
        }
        callProcedure("update_trial_rates", ids);
        callProcedure("update_group_trial_rates", ids);
    }

    public Collection<KeyValue> populateRateValues() {
        Collection<KeyValue> result = new ArrayList<KeyValue>(5);
        for (int i = 2; i >= -2; --i) {
            String keyValue = new Integer(i).toString();
            result.add(new DefaultKeyValue(keyValue, keyValue));
        }
        return result;
    }

    private double getWeightForResult(int position) {
        double[] weights = config.getResultWeights();
        return position > weights.length ? weights[weights.length - 1] : weights[position - 1];
    }

    private void callProcedure(final String name, final Long[] rateIds) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Connection connection = dataSource.getConnection();
                    Array ids = connection.createArrayOf("bigint", rateIds);
                    final CallableStatement cstmt = connection.prepareCall(String.format("{call %s(?)}", name));
                    cstmt.setArray(1, ids);
                    cstmt.execute();
                } catch (SQLException e) {
                    log.warn("Error when calling procedure", e);
                }
            }
        }.start();
    }
}
