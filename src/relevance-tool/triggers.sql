-- GROUP TRIAL
CREATE OR REPLACE FUNCTION delete_group_trial() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM group_trial_rate WHERE trial=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_group_trial_trigger ON group_trial;
CREATE TRIGGER delete_group_trial_trigger BEFORE DELETE ON group_trial
FOR EACH ROW EXECUTE PROCEDURE delete_group_trial();

-- GROUP TRIAL RATE
CREATE OR REPLACE FUNCTION delete_group_trial_rate() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM group_rate_item WHERE group_trial_rate=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_group_trial_rate_trigger ON group_trial_rate;
CREATE TRIGGER delete_group_trial_rate_trigger BEFORE DELETE ON group_trial_rate
FOR EACH ROW EXECUTE PROCEDURE delete_group_trial_rate();

-- GROUP TRIAL RATE ITEM
CREATE OR REPLACE FUNCTION delete_group_rate_item() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM group_rate_detailed_item WHERE group_rate_item=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_group_rate_item_trigger ON group_rate_item;
CREATE TRIGGER delete_group_rate_item_trigger BEFORE DELETE ON group_rate_item
FOR EACH ROW EXECUTE PROCEDURE delete_group_rate_item();

-- TRIAL
CREATE OR REPLACE FUNCTION delete_trial() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM trial_rate WHERE trial=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_trial_trigger ON trial;
CREATE TRIGGER delete_trial_trigger BEFORE DELETE ON trial
FOR EACH ROW EXECUTE PROCEDURE delete_trial();

-- TRIAL RATE
CREATE OR REPLACE FUNCTION delete_trial_rate() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM rate_item WHERE trial_rate=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_trial_rate_trigger ON trial_rate;
CREATE TRIGGER delete_trial_rate_trigger BEFORE DELETE ON trial_rate
FOR EACH ROW EXECUTE PROCEDURE delete_trial_rate();

-- RATE
CREATE OR REPLACE FUNCTION delete_rate() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM rate_item WHERE rate=OLD.id;
    DELETE FROM group_rate_detailed_item WHERE rate=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_rate_trigger ON rate;
CREATE TRIGGER delete_rate_trigger BEFORE DELETE ON rate
FOR EACH ROW EXECUTE PROCEDURE delete_rate();

-- DOCUMENT
CREATE OR REPLACE FUNCTION delete_document() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM rate WHERE document=OLD.id;
    DELETE FROM rate_item WHERE document=OLD.id;
    DELETE FROM group_rate_detailed_item WHERE document=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_document_trigger ON document;
CREATE TRIGGER delete_document_trigger BEFORE DELETE ON document
FOR EACH ROW EXECUTE PROCEDURE delete_document();

-- PHRASE
CREATE OR REPLACE FUNCTION delete_phrase() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM weighted_phrase WHERE phrase=OLD.id;
    DELETE FROM trial WHERE phrase=OLD.id;
    DELETE FROM rate WHERE phrase=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_phrase_trigger ON phrase;
CREATE TRIGGER delete_phrase_trigger BEFORE DELETE ON phrase
FOR EACH ROW EXECUTE PROCEDURE delete_phrase();

-- PHRASE GROUP
CREATE OR REPLACE FUNCTION delete_phrase_group() RETURNS TRIGGER AS
$BODY$
  BEGIN
    DELETE FROM weighted_phrase WHERE phrase_group=OLD.id;
    DELETE FROM group_trial WHERE phrase_group=OLD.id;
    RETURN OLD;
  END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS delete_phrase_group_trigger ON phrase_group;
CREATE TRIGGER delete_phrase_group_trigger BEFORE DELETE ON phrase_group
FOR EACH ROW EXECUTE PROCEDURE delete_phrase_group();
