-- group_rate_detailed_item
DROP INDEX group_rate_detailed_item_document_index;
CREATE INDEX group_rate_detailed_item_document_index ON group_rate_detailed_item USING btree(document);
DROP INDEX group_rate_detailed_item_group_rate_item_index;
CREATE INDEX group_rate_detailed_item_group_rate_item_index ON group_rate_detailed_item USING btree(group_rate_item);
DROP INDEX group_rate_detailed_item_phrase_index;
CREATE INDEX group_rate_detailed_item_phrase_index ON group_rate_detailed_item USING btree(phrase);
DROP INDEX group_rate_detailed_item_rate_index;
CREATE INDEX group_rate_detailed_item_rate_index ON group_rate_detailed_item USING btree(rate);

-- group_rate_item
DROP INDEX group_rate_item_phrase_index;
CREATE INDEX group_rate_item_phrase_index ON group_rate_item USING btree(phrase);
DROP INDEX group_rate_item_group_trial_rate_index;
CREATE INDEX group_rate_item_group_trial_rate_index ON group_rate_item USING btree(group_trial_rate);

-- group_trial
DROP INDEX group_trial_algorithm_index;
CREATE INDEX group_trial_algorithm_index ON group_trial USING btree(algorithm);
DROP INDEX group_trial_phrase_group_index;
CREATE INDEX group_trial_phrase_group_index ON group_trial USING btree(phrase_group);
DROP INDEX group_trial_server_index;
CREATE INDEX group_trial_server_index ON group_trial USING btree(server);
DROP INDEX group_trial_web_user_index;
CREATE INDEX group_trial_web_user_index ON group_trial USING btree(web_user);

-- group_trial_rate
DROP INDEX group_trial_rate_trial_index;
CREATE INDEX group_trial_rate_trial_index ON group_trial_rate USING btree(trial);
DROP INDEX group_trial_rate_web_user_index;
CREATE INDEX group_trial_rate_web_user_index ON group_trial_rate USING btree(web_user);

-- phrase
DROP INDEX phrase_web_user_index;
CREATE INDEX phrase_web_user_index ON phrase USING btree(web_user);

-- phrase_group
DROP INDEX phrase_group_web_user_index;
CREATE INDEX phrase_group_web_user_index ON phrase_group USING btree(web_user);

-- rate
DROP INDEX rate_document_index;
CREATE INDEX rate_document_index ON rate USING btree(document);
DROP INDEX rate_phrase_index;
CREATE INDEX rate_phrase_index ON rate USING btree(phrase);

-- rate_item
DROP INDEX rate_item_phrase_index;
CREATE INDEX rate_item_phrase_index ON rate_item USING btree(phrase);
DROP INDEX rate_item_document_index;
CREATE INDEX rate_item_document_index ON rate_item USING btree(document);
DROP INDEX rate_item_rate_index;
CREATE INDEX rate_item_rate_index ON rate_item USING btree(rate);
DROP INDEX rate_item_trial_rate_index;
CREATE INDEX rate_item_trial_rate_index ON rate_item USING btree(trial_rate);

-- trial
DROP INDEX trial_phrase_index;
CREATE INDEX trial_phrase_index ON trial USING btree(phrase);
DROP INDEX trial_server_index;
CREATE INDEX trial_server_index ON trial USING btree(server);
DROP INDEX trial_algorithm_index;
CREATE INDEX trial_algorithm_index ON trial USING btree(algorithm);
DROP INDEX trial_web_user_index;
CREATE INDEX trial_web_user_index ON trial USING btree(web_user);

-- trial_rate
DROP INDEX trial_rate_trial_index;
CREATE INDEX trial_rate_trial_index ON trial_rate USING btree(trial);
DROP INDEX trial_rate_web_user_index;
CREATE INDEX trial_rate_web_user_index ON trial_rate USING btree(web_user);

-- web_user
DROP INDEX web_user_web_user_role_index;
CREATE INDEX web_user_web_user_role_index ON web_user USING btree(user_role);

-- weighted_phrase
DROP INDEX weighted_phrase_phrase_index;
CREATE INDEX weighted_phrase_phrase_index ON weighted_phrase USING btree(phrase);
DROP INDEX weighted_phrase_phrase_group_index;
CREATE INDEX weighted_phrase_phrase_group_index ON weighted_phrase USING btree(phrase_group);
