package com.findwise.xkom.querycompletion.controller;


import com.findwise.xkom.querycompletion.domain.Suggestion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(path="/completer")
public class Completer {


    @GetMapping(value = "/complete/{phrase}")
    @ResponseBody
    public List<Suggestion> complete(@PathVariable final String phrase){
        List<Suggestion> suggestions = Arrays.asList(new Suggestion("ala ma kota",2),new Suggestion("kra kre mija",30));
        return suggestions;
    }
}
