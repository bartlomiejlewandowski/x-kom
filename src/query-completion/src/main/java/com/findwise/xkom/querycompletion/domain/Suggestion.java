package com.findwise.xkom.querycompletion.domain;

public class Suggestion {

    private String content;
    private int number;

    public String getContent() {
        return content;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Suggestion(String content, int number) {
        this.content = content;
        this.number = number;
    }
}
